// Run a A4998 Stepstick from an Arduino
int x; 
#define BAUD (9600)
#define MOTOR_ONE_ENABLE_PIN 6
#define MOTOR_ONE_STEP_PIN 5
#define MOTOR_ONE_DIRECTION_PIN 4

void setup() 
{
  Serial.begin(9600);
  Serial1.begin(9600);
  pinMode(MOTOR_ONE_ENABLE_PIN,OUTPUT); // Enable
  pinMode(MOTOR_ONE_STEP_PIN,OUTPUT); // Step
  pinMode(MOTOR_ONE_DIRECTION_PIN,OUTPUT); // Dir
  digitalWrite(MOTOR_ONE_ENABLE_PIN,LOW); // Set Enable low
}

void loop() 
{
  digitalWrite(MOTOR_ONE_DIRECTION_PIN,HIGH); // Set Dir high
  Serial.println("Loop 200 steps (1 rev)");
  for(x = 0; x < 200; x++) // Loop 200 times
  {
    digitalWrite(MOTOR_ONE_STEP_PIN,HIGH); // Output high
    delay(10); // Wait
    digitalWrite(MOTOR_ONE_STEP_PIN,LOW); // Output low
    delay(100); // Wait
  }
  Serial.println("Pause");
  delay(1000); // pause one second
  digitalWrite(MOTOR_ONE_DIRECTION_PIN,LOW); // Set Dir low
  for(x = 0; x < 200; x++) // Loop 200 times
  {
    digitalWrite(MOTOR_ONE_STEP_PIN,HIGH); // Output high
    delay(10); // Wait
    digitalWrite(MOTOR_ONE_STEP_PIN,LOW); // Output low
    delay(100); // Wait
  }
  Serial.println("Pause");
  delay(1000); // pause one second
}
