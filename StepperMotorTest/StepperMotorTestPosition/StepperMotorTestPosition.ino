// Run a A4998 Stepstick from an Arduino
int x; 
#define BAUD (9600)
#define MOTOR_X_ENABLE_PIN 22
#define MOTOR_X_STEP_PIN 23
#define MOTOR_X_DIRECTION_PIN 24
#define MOTOR_X_ZERO_SWITCH 25

const byte numChars = 32;
char receivedChars[numChars]; // an array to store the received data

boolean newData = false;
char delimiter[3] = ", ";

void recvWithEndMarker() {
  static byte ndx = 0;
  char endMarker = '\n';
  char rc;
 
  if (Serial.available() > 0) {
    while (Serial.available() > 0 && newData == false) {
      rc = Serial.read();
      if (rc != endMarker) {
        //Serial.println("!");
        receivedChars[ndx] = rc;
        ndx++;
        if (ndx >= numChars) {
          ndx = numChars - 1;
        }
      }
      else {
        receivedChars[ndx] = '\0'; // terminate the string
        ndx = 0;
        newData = true;
        
      }
    }
  }
}

void setup() 
{
  Serial.begin(9600);
  Serial1.begin(9600);
  pinMode(MOTOR_X_ENABLE_PIN,OUTPUT); // Enable
  pinMode(MOTOR_X_STEP_PIN,OUTPUT); // Step
  pinMode(MOTOR_X_DIRECTION_PIN,OUTPUT); // Dir
  pinMode(MOTOR_X_ZERO_SWITCH, INPUT); // Zero
  digitalWrite(MOTOR_X_ENABLE_PIN,LOW); // Set Enable low
}

int locationX = 0;
int requestedPositionX = 0;

void loop() 
{
  if (Serial.available() > 0){// If data available
    recvWithEndMarker();
    if (newData == true){
      newData = false;
      char * token;
      token = strtok(receivedChars,delimiter);
      if ((!strcmp(token,"p"))|(!strcmp(token,"P"))|(!strcmp(token,"position"))|(!strcmp(token,"Position"))){
        token = strtok(NULL,delimiter);
        Serial.println(token);
        requestedPositionX = atoi(token);
        Serial.print("Requested ");
        Serial.print(requestedPositionX);
        Serial.print(" At ");
        Serial.println(locationX);
        if (locationX == requestedPositionX){
          
        }
        else if (locationX < requestedPositionX){
          digitalWrite(MOTOR_X_DIRECTION_PIN,LOW);
          Serial.println("MOTOR_DIRECTION UP");
          for(locationX; locationX < requestedPositionX; ++locationX) // Loop 200 times
          {
            digitalWrite(MOTOR_X_STEP_PIN,HIGH); // Output high
            delay(1); // Wait
            digitalWrite(MOTOR_X_STEP_PIN,LOW); // Output low
            delay(1); // Wait
          }
        }
        else if (locationX > requestedPositionX){
          Serial.println("MOTOR_DIRECTION DOWN");
          digitalWrite(MOTOR_X_DIRECTION_PIN,HIGH);
          for(locationX; locationX > requestedPositionX; --locationX) // Loop 200 times
          {
            digitalWrite(MOTOR_X_STEP_PIN,HIGH); // Output high
            delay(1); // Wait
            digitalWrite(MOTOR_X_STEP_PIN,LOW); // Output low
            delay(1); // Wait
          }
        }
        Serial.print("At locationX ");
        Serial.println(locationX);
      }
      else if ((!strcmp(token,"z"))|(!strcmp(token,"Z"))|(!strcmp(token,"Zero"))|(!strcmp(token,"zero"))){// go to zero switch is hit in each axis
        Serial.println("Zeroing");
        digitalWrite(MOTOR_X_DIRECTION_PIN,HIGH); // Set Dir high
        while (digitalRead(MOTOR_X_ZERO_SWITCH) == LOW){
          digitalWrite(MOTOR_X_STEP_PIN,HIGH); // Output high
          delay(1); // Wait
          digitalWrite(MOTOR_X_STEP_PIN,LOW); // Output low
          delay(1); // Wait
        }
        locationX = 0;
      }
      else{
        Serial.print("Unrecognized command");
      }
    }
  }
  Serial.println("Delay");
  delay(1000);
}
