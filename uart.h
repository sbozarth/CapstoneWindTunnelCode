#ifndef UART_HEADER_FILE
#define UART_HEADER_FILE

#include <stdint.h>

extern unsigned char queue[1024];
extern unsigned char recieveQueue[1024];

void uart_init();
void uart_send(unsigned char* data);
unsigned char uart_recieve();
unsigned char uart_get_char();
uint8_t uart_has_data();

#endif