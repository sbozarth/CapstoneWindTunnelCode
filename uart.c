/************************************************************************/
/* Name: Daniel Scott Bozarth                                           */
/* Date: 2018-09-26                                                     */
/*                                                                      */
/* Hardware Requirements:                                               */
/* N/A                                                                  */
/*                                                                      */
/* Build Requirements:                                                  */
/* N/A                                                                  */
/* Built using the following commands                                   */
/* make                                                                 */
/*                                                                      */
/* Modification History:                                                */
/* Initial revision (2018-09-26):                                       */
/************************************************************************/

#include "uart.h"
#include <avr/sleep.h>

#include <avr/io.h> 
#include <avr/interrupt.h>

#define F_CPU 8000000
#define BAUD 9600
#include <util/setbaud.h>

unsigned char queue[1024];
unsigned char recieveQueue[1024];
uint8_t bufferLoc = 0;
uint8_t recieveBufferLoc = 0;
uint8_t lastSent = 0;
uint8_t lastRecieved = 0;

#if 0
// Transmission Complete
ISR(USART0_TX_vect){
	
}
#endif

//Byte recieved
ISR(USART0_RX_vect){
	if(recieveBufferLoc != lastRecieved){
		lastRecieved +=1;
		recieveQueue[lastRecieved] = UDR0;
	}
}

// UDR empty
ISR(USART0_UDRE_vect){
	if (bufferLoc != lastSent){
		lastSent +=1;
		UDR0 = queue[lastSent];
	}
	
}


void uart_init(){
	UCSR0C &= ~(1<<USBS0); // 1 stop bit
	UBRR0H = UBRRH_VALUE;
	UBRR0L = UBRRL_VALUE;
	#if USE_2X
	UCSR0A |= (1 << U2X0);
	#else
	UCSR0A &= ~(1 << U2X0);
	#endif
	UCSR0B = (1<<RXEN0) | (1<<TXEN0);
	UCSR0B |= (1<<UDRIE0) | (1<<RXCIE0); //| (1<<TXCIE0);
	UDR0 = '!';
}

void uart_send(unsigned char* data){
	cli();
	if(bufferLoc-lastSent < 10){
		for (uint8_t i = 0; i < 16; ++i){
			if(data[i]){
				bufferLoc +=1; 
				queue[bufferLoc] = data[i];
			}
			else{
				break;
			}
		}
	}
	sei();
}

uint8_t uart_has_data(){
	uint8_t hasData = 0;
	if (recieveBufferLoc != lastRecieved){
		hasData = 1;
	}
	return hasData;
}

unsigned char uart_get_char(){
	recieveBufferLoc += 1;
	return recieveQueue[recieveBufferLoc-1];
}

unsigned char uart_recieve(){
	/* Wait for data to be received */
   while ( !(UCSR0A & (1<<RXC0)) )
      ;
   /* Get and return received data from buffer */
   return UDR0;
}