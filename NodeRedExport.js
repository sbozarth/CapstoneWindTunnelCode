[
    {
        "id": "297f7fc8.3b546",
        "type": "tab",
        "label": "WindTunnel",
        "disabled": false,
        "info": ""
    },
    {
        "id": "806614ac.0dfe68",
        "type": "tab",
        "label": "PiStatus",
        "disabled": false,
        "info": ""
    },
    {
        "id": "c8f417ae.6df528",
        "type": "tab",
        "label": "Info",
        "disabled": false,
        "info": ""
    },
    {
        "id": "6cfd549c.4d07cc",
        "type": "serial-port",
        "z": "",
        "serialport": "/dev/ttyACM0",
        "serialbaud": "115200",
        "databits": "8",
        "parity": "none",
        "stopbits": "1",
        "newline": "\\n",
        "bin": "false",
        "out": "char",
        "addchar": false,
        "responsetimeout": "10000"
    },
    {
        "id": "3501ff0.e69d102",
        "type": "ui_tab",
        "z": "",
        "name": "WindTunnel",
        "icon": "dashboard",
        "order": 1,
        "disabled": false,
        "hidden": false
    },
    {
        "id": "6cfbd745.8e4668",
        "type": "ui_group",
        "z": "",
        "name": "Serial Data",
        "tab": "3501ff0.e69d102",
        "order": 3,
        "disp": true,
        "width": "10",
        "collapse": false
    },
    {
        "id": "3a000dce.a2f5e2",
        "type": "ui_base",
        "theme": {
            "name": "theme-dark",
            "lightTheme": {
                "default": "#0094CE",
                "baseColor": "#0094CE",
                "baseFont": "-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif",
                "edited": true,
                "reset": false
            },
            "darkTheme": {
                "default": "#097479",
                "baseColor": "#564e83",
                "baseFont": "-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif",
                "edited": true,
                "reset": false
            },
            "customTheme": {
                "name": "Untitled Theme 1",
                "default": "#4B7930",
                "baseColor": "#4B7930",
                "baseFont": "-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif"
            },
            "themeState": {
                "base-color": {
                    "default": "#097479",
                    "value": "#564e83",
                    "edited": true
                },
                "page-titlebar-backgroundColor": {
                    "value": "#564e83",
                    "edited": false
                },
                "page-backgroundColor": {
                    "value": "#111111",
                    "edited": false
                },
                "page-sidebar-backgroundColor": {
                    "value": "#000000",
                    "edited": false
                },
                "group-textColor": {
                    "value": "#7b72ab",
                    "edited": false
                },
                "group-borderColor": {
                    "value": "#555555",
                    "edited": false
                },
                "group-backgroundColor": {
                    "value": "#333333",
                    "edited": false
                },
                "widget-textColor": {
                    "value": "#eeeeee",
                    "edited": false
                },
                "widget-backgroundColor": {
                    "value": "#564e83",
                    "edited": false
                },
                "widget-borderColor": {
                    "value": "#333333",
                    "edited": false
                },
                "base-font": {
                    "value": "-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif"
                }
            },
            "angularTheme": {
                "primary": "indigo",
                "accents": "blue",
                "warn": "red",
                "background": "grey"
            }
        },
        "site": {
            "name": "Wind Tunnel Dashboard",
            "hideToolbar": "false",
            "allowSwipe": "false",
            "lockMenu": "false",
            "allowTempTheme": "true",
            "dateFormat": "DD/MM/YYYY",
            "sizes": {
                "sx": 48,
                "sy": 48,
                "gx": 6,
                "gy": 6,
                "cx": 6,
                "cy": 6,
                "px": 0,
                "py": 0
            }
        }
    },
    {
        "id": "6474df5.cdcb92",
        "type": "ui_tab",
        "z": "",
        "name": "Pi Status",
        "icon": "dashboard",
        "order": 3,
        "disabled": false,
        "hidden": false
    },
    {
        "id": "d3704d72.289f3",
        "type": "ui_group",
        "z": "",
        "name": "Graph 0",
        "tab": "6474df5.cdcb92",
        "order": 2,
        "disp": false,
        "width": "14",
        "collapse": false
    },
    {
        "id": "2d3aff8b.56348",
        "type": "ui_group",
        "z": "",
        "name": "Airflow",
        "tab": "3501ff0.e69d102",
        "order": 1,
        "disp": true,
        "width": "15",
        "collapse": false
    },
    {
        "id": "bbbba8d8.bfba18",
        "type": "ui_group",
        "z": "",
        "name": "Gauges",
        "tab": "6474df5.cdcb92",
        "order": 1,
        "disp": false,
        "width": "6",
        "collapse": false
    },
    {
        "id": "d752012.579b4",
        "type": "ui_group",
        "z": "",
        "name": "Graph 1",
        "tab": "6474df5.cdcb92",
        "order": 3,
        "disp": false,
        "width": "6",
        "collapse": false
    },
    {
        "id": "468e4795.b18ba8",
        "type": "ui_group",
        "z": "",
        "name": "Graph 2",
        "tab": "6474df5.cdcb92",
        "order": 4,
        "disp": false,
        "width": "6",
        "collapse": false
    },
    {
        "id": "51c69a15.4dfea4",
        "type": "ui_group",
        "z": "",
        "name": "Atmospheric",
        "tab": "3501ff0.e69d102",
        "order": 4,
        "disp": true,
        "width": "10",
        "collapse": true
    },
    {
        "id": "c4fedc5c.a4592",
        "type": "ui_group",
        "z": "",
        "name": "Position",
        "tab": "3501ff0.e69d102",
        "order": 2,
        "disp": true,
        "width": "15",
        "collapse": false
    },
    {
        "id": "1aa61a2f.00f006",
        "type": "ui_spacer",
        "name": "spacer",
        "group": "c4fedc5c.a4592",
        "order": 4,
        "width": "6",
        "height": 1
    },
    {
        "id": "85392e69.aedbb",
        "type": "ui_spacer",
        "name": "spacer",
        "group": "c4fedc5c.a4592",
        "order": 10,
        "width": "6",
        "height": 1
    },
    {
        "id": "4e01d49e.e843fc",
        "type": "ui_spacer",
        "name": "spacer",
        "group": "2d3aff8b.56348",
        "order": 13,
        "width": "2",
        "height": "4"
    },
    {
        "id": "92be8eed.e33d7",
        "type": "ui_spacer",
        "name": "spacer",
        "group": "2d3aff8b.56348",
        "order": 16,
        "width": 1,
        "height": 1
    },
    {
        "id": "7eb91212.78254c",
        "type": "ui_spacer",
        "name": "spacer",
        "group": "2d3aff8b.56348",
        "order": 18,
        "width": "3",
        "height": 1
    },
    {
        "id": "5b7d2916.9a5e78",
        "type": "ui_group",
        "z": "",
        "name": "Airflow",
        "tab": "f0d18a65.936aa8",
        "order": 2,
        "disp": true,
        "width": "12",
        "collapse": false
    },
    {
        "id": "1d34d093.42489f",
        "type": "ui_spacer",
        "name": "spacer",
        "group": "2d3aff8b.56348",
        "order": 9,
        "width": "2",
        "height": "4"
    },
    {
        "id": "52c4ce76.c7162",
        "type": "ui_spacer",
        "name": "spacer",
        "group": "2d3aff8b.56348",
        "order": 11,
        "width": "1",
        "height": "4"
    },
    {
        "id": "267b55d6.1719ba",
        "type": "ui_spacer",
        "name": "spacer",
        "group": "2d3aff8b.56348",
        "order": 3,
        "width": 1,
        "height": "4"
    },
    {
        "id": "c40d2fb2.78d3b",
        "type": "ui_spacer",
        "name": "spacer",
        "group": "2d3aff8b.56348",
        "order": 1,
        "width": "2",
        "height": "4"
    },
    {
        "id": "3e30e066.26646",
        "type": "ui_spacer",
        "name": "spacer",
        "group": "2d3aff8b.56348",
        "order": 5,
        "width": "2",
        "height": "4"
    },
    {
        "id": "f3055b3a.8bd9c8",
        "type": "ui_spacer",
        "name": "spacer",
        "group": "2d3aff8b.56348",
        "order": 14,
        "width": "3",
        "height": 1
    },
    {
        "id": "f0d18a65.936aa8",
        "type": "ui_tab",
        "z": "",
        "name": "Charts",
        "icon": "dashboard",
        "order": 2,
        "disabled": false,
        "hidden": false
    },
    {
        "id": "689f4873.2532b8",
        "type": "ui_group",
        "z": "",
        "name": "Velocity",
        "tab": "f0d18a65.936aa8",
        "order": 1,
        "disp": true,
        "width": "12",
        "collapse": false
    },
    {
        "id": "edc8336a.c696a",
        "type": "ui_group",
        "z": "",
        "name": "Temperature",
        "tab": "f0d18a65.936aa8",
        "disp": true,
        "width": "12",
        "collapse": false
    },
    {
        "id": "7da12b1d.42cf34",
        "type": "file",
        "z": "297f7fc8.3b546",
        "name": "",
        "filename": "/home/pi/test/log.csv",
        "appendNewline": false,
        "createDir": false,
        "overwriteFile": "false",
        "x": 725,
        "y": 550,
        "wires": [
            []
        ]
    },
    {
        "id": "d389b25b.8af27",
        "type": "ui_text",
        "z": "297f7fc8.3b546",
        "group": "6cfbd745.8e4668",
        "order": 2,
        "width": 0,
        "height": 0,
        "name": "Last Log",
        "label": "Last Log",
        "format": "{{msg.payload}}",
        "layout": "col-center",
        "x": 760,
        "y": 450,
        "wires": []
    },
    {
        "id": "126da37e.25487d",
        "type": "serial out",
        "z": "297f7fc8.3b546",
        "name": "Send Serial",
        "serial": "6cfd549c.4d07cc",
        "x": 745,
        "y": 1000,
        "wires": []
    },
    {
        "id": "8d19a52.4c88758",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Add Newline",
        "func": "msg.payload = msg.payload.msg+\"\\n\";\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 490,
        "y": 760,
        "wires": [
            [
                "126da37e.25487d",
                "682e8061.c3b05"
            ]
        ]
    },
    {
        "id": "b47ca416.881d58",
        "type": "exec",
        "z": "806614ac.0dfe68",
        "command": "/opt/vc/bin/vcgencmd measure_temp",
        "addpay": false,
        "append": "",
        "useSpawn": "false",
        "timer": "",
        "oldrc": false,
        "name": "",
        "x": 410,
        "y": 380,
        "wires": [
            [
                "347e9e14.2932d2"
            ],
            [],
            []
        ]
    },
    {
        "id": "df4ad4bc.88eaa8",
        "type": "ui_gauge",
        "z": "806614ac.0dfe68",
        "name": "",
        "group": "bbbba8d8.bfba18",
        "order": 1,
        "width": 0,
        "height": 0,
        "gtype": "gage",
        "title": "Temperature",
        "label": "Celcius",
        "format": "{{value}}",
        "min": "30",
        "max": "100",
        "colors": [
            "#00b500",
            "#e6e600",
            "#ca3838"
        ],
        "seg1": "60",
        "seg2": "80",
        "x": 830,
        "y": 360,
        "wires": []
    },
    {
        "id": "347e9e14.2932d2",
        "type": "function",
        "z": "806614ac.0dfe68",
        "name": "Temp",
        "func": "msg.payload = msg.payload.replace(\"temp=\",\"\").replace(\"'C\\n\",\"\"); \nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 670,
        "y": 360,
        "wires": [
            [
                "df4ad4bc.88eaa8",
                "8962f3f4.28ff2"
            ]
        ]
    },
    {
        "id": "66cc7571.9a830c",
        "type": "inject",
        "z": "806614ac.0dfe68",
        "name": "Time",
        "topic": "",
        "payload": "",
        "payloadType": "date",
        "repeat": "5",
        "crontab": "",
        "once": false,
        "onceDelay": 0.1,
        "x": 150,
        "y": 380,
        "wires": [
            [
                "b47ca416.881d58"
            ]
        ]
    },
    {
        "id": "8962f3f4.28ff2",
        "type": "ui_chart",
        "z": "806614ac.0dfe68",
        "name": "10 Min",
        "group": "d3704d72.289f3",
        "order": 1,
        "width": 0,
        "height": 0,
        "label": "Temperature Past 10 Min",
        "chartType": "line",
        "legend": "false",
        "xformat": "HH:mm",
        "interpolate": "linear",
        "nodata": "",
        "dot": false,
        "ymin": "",
        "ymax": "",
        "removeOlder": "10",
        "removeOlderPoints": "",
        "removeOlderUnit": "60",
        "cutout": 0,
        "useOneColor": false,
        "colors": [
            "#1f77b4",
            "#aec7e8",
            "#ff7f0e",
            "#2ca02c",
            "#98df8a",
            "#d62728",
            "#ff9896",
            "#9467bd",
            "#c5b0d5"
        ],
        "useOldStyle": false,
        "x": 850,
        "y": 400,
        "wires": [
            [],
            []
        ]
    },
    {
        "id": "27c73b2.c9f05c4",
        "type": "ui_chart",
        "z": "806614ac.0dfe68",
        "name": "Week",
        "group": "468e4795.b18ba8",
        "order": 2,
        "width": 0,
        "height": 0,
        "label": "Temperature Past Week",
        "chartType": "line",
        "legend": "false",
        "xformat": "MM-DD",
        "interpolate": "linear",
        "nodata": "",
        "dot": false,
        "ymin": "",
        "ymax": "",
        "removeOlder": 1,
        "removeOlderPoints": "300",
        "removeOlderUnit": "604800",
        "cutout": 0,
        "useOneColor": false,
        "colors": [
            "#1f77b4",
            "#aec7e8",
            "#ff7f0e",
            "#2ca02c",
            "#98df8a",
            "#d62728",
            "#ff9896",
            "#9467bd",
            "#c5b0d5"
        ],
        "useOldStyle": false,
        "x": 850,
        "y": 460,
        "wires": [
            [],
            []
        ]
    },
    {
        "id": "dbe5050e.7808a8",
        "type": "exec",
        "z": "806614ac.0dfe68",
        "command": "/opt/vc/bin/vcgencmd measure_temp",
        "addpay": false,
        "append": "",
        "useSpawn": "false",
        "timer": "",
        "oldrc": false,
        "name": "",
        "x": 410,
        "y": 480,
        "wires": [
            [
                "3f609628.d2daaa"
            ],
            [],
            []
        ]
    },
    {
        "id": "3f609628.d2daaa",
        "type": "function",
        "z": "806614ac.0dfe68",
        "name": "Temp",
        "func": "msg.payload = msg.payload.replace(\"temp=\",\"\").replace(\"'C\\n\",\"\"); \nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 670,
        "y": 460,
        "wires": [
            [
                "27c73b2.c9f05c4"
            ]
        ]
    },
    {
        "id": "65bdf3fe.f0d67c",
        "type": "inject",
        "z": "806614ac.0dfe68",
        "name": "Time",
        "topic": "",
        "payload": "",
        "payloadType": "date",
        "repeat": "2016",
        "crontab": "",
        "once": false,
        "onceDelay": 0.1,
        "x": 150,
        "y": 480,
        "wires": [
            [
                "dbe5050e.7808a8"
            ]
        ]
    },
    {
        "id": "53f1bdf2.3601e4",
        "type": "ui_form",
        "z": "297f7fc8.3b546",
        "name": "Enter Serial",
        "label": "Enter Serial",
        "group": "6cfbd745.8e4668",
        "order": 1,
        "width": 0,
        "height": 0,
        "options": [
            {
                "label": "",
                "value": "msg",
                "type": "text",
                "required": true
            }
        ],
        "formValue": {
            "msg": ""
        },
        "payload": "",
        "submit": "send",
        "cancel": "cancel",
        "topic": "",
        "x": 70,
        "y": 750,
        "wires": [
            [
                "8d19a52.4c88758"
            ]
        ]
    },
    {
        "id": "c515d478.ab6718",
        "type": "debug",
        "z": "297f7fc8.3b546",
        "name": "Serial Command",
        "active": true,
        "tosidebar": true,
        "console": false,
        "tostatus": false,
        "complete": "payload",
        "x": 710,
        "y": 760,
        "wires": []
    },
    {
        "id": "52511212.d42d4c",
        "type": "switch",
        "z": "297f7fc8.3b546",
        "name": "LOG output",
        "property": "payload",
        "propertyType": "msg",
        "rules": [
            {
                "t": "regex",
                "v": "LOG",
                "vt": "str",
                "case": false
            }
        ],
        "checkall": "true",
        "repair": false,
        "outputs": 1,
        "x": 290,
        "y": 560,
        "wires": [
            [
                "4be53260.89faec"
            ]
        ]
    },
    {
        "id": "5feabb48.688b24",
        "type": "file",
        "z": "297f7fc8.3b546",
        "name": "",
        "filename": "/home/pi/test/debug.txt",
        "appendNewline": false,
        "createDir": false,
        "overwriteFile": "false",
        "x": 710,
        "y": 625,
        "wires": [
            []
        ]
    },
    {
        "id": "d8df5f1a.cc349",
        "type": "switch",
        "z": "297f7fc8.3b546",
        "name": "DEBUG Output",
        "property": "payload",
        "propertyType": "msg",
        "rules": [
            {
                "t": "regex",
                "v": "DEBUG",
                "vt": "str",
                "case": false
            }
        ],
        "checkall": "true",
        "repair": false,
        "outputs": 1,
        "x": 300,
        "y": 620,
        "wires": [
            [
                "b58f3a6.73d2cc8"
            ]
        ]
    },
    {
        "id": "4be53260.89faec",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Strip Tag",
        "func": "msg.payload = msg.payload.replace(/^[^:]+:/,\"\");\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 470,
        "y": 560,
        "wires": [
            [
                "7da12b1d.42cf34",
                "d389b25b.8af27",
                "1be0d350.bcb2fd"
            ]
        ]
    },
    {
        "id": "b58f3a6.73d2cc8",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Strip Tag",
        "func": "msg.payload = msg.payload.replace(/^[^:]+:/,\"\");\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 470,
        "y": 620,
        "wires": [
            [
                "5feabb48.688b24"
            ]
        ]
    },
    {
        "id": "e70bc8e.c748138",
        "type": "ui_form",
        "z": "297f7fc8.3b546",
        "name": "Enter Airflow",
        "label": "Enter Airflow",
        "group": "2d3aff8b.56348",
        "order": 19,
        "width": "15",
        "height": "1",
        "options": [
            {
                "label": "",
                "value": "airflow",
                "type": "text",
                "required": true
            }
        ],
        "formValue": {
            "airflow": ""
        },
        "payload": "",
        "submit": "send",
        "cancel": "cancel",
        "topic": "",
        "x": 70,
        "y": 820,
        "wires": [
            [
                "cfc96acf.1d89c8"
            ]
        ]
    },
    {
        "id": "1be0d350.bcb2fd",
        "type": "csv",
        "z": "297f7fc8.3b546",
        "name": "",
        "sep": ",",
        "hdrin": "",
        "hdrout": "",
        "multi": "one",
        "ret": "\\n",
        "temp": "",
        "skip": "0",
        "x": 775,
        "y": 500,
        "wires": [
            [
                "759cd554.8b74ac",
                "b4c25aa5.1a4f98",
                "a85892ff.91033",
                "77fa4f53.f1547",
                "7529d65f.1644d8",
                "eed1338.9fa01d",
                "125db5b2.f8bb6a",
                "9118397e.4d9e88",
                "1a0b6ea4.ece7d1",
                "521c8c0b.b1d6c4",
                "6ff0dbc.9e05724",
                "da00b4c2.2bbbd8",
                "9ea05cc1.5e5af",
                "3a41679a.148da8",
                "81a06b52.35fb08"
            ]
        ]
    },
    {
        "id": "b4c25aa5.1a4f98",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Get Requested Airflow",
        "func": "msg.payload = msg.payload.col2\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 1060,
        "y": 75,
        "wires": [
            [
                "9bffa87a.b56c68"
            ]
        ]
    },
    {
        "id": "759cd554.8b74ac",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Get Current Airflow",
        "func": "if(msg.payload != \" \"){\n    msg.payload = msg.payload.col3\n    return msg;\n}\nelse{\n    msg.payload = null;\n}",
        "outputs": 1,
        "noerr": 0,
        "x": 1040,
        "y": 125,
        "wires": [
            [
                "48e4fc29.1cc784"
            ]
        ]
    },
    {
        "id": "9bffa87a.b56c68",
        "type": "ui_gauge",
        "z": "297f7fc8.3b546",
        "name": "Display Requested Airflow",
        "group": "2d3aff8b.56348",
        "order": 4,
        "width": "5",
        "height": "4",
        "gtype": "gage",
        "title": "Requested Airflow",
        "label": "m^3/min",
        "format": "{{value}}",
        "min": 0,
        "max": "3.3",
        "colors": [
            "#d70d2f",
            "#d9bb48",
            "#d70d2f"
        ],
        "seg1": "0",
        "seg2": "3.3",
        "x": 1340,
        "y": 75,
        "wires": []
    },
    {
        "id": "1ab44c6c.6ad4d4",
        "type": "ui_gauge",
        "z": "297f7fc8.3b546",
        "name": "Display Actual Airflow",
        "group": "2d3aff8b.56348",
        "order": 2,
        "width": "5",
        "height": "4",
        "gtype": "gage",
        "title": "Actual Airflow",
        "label": "m^3/min",
        "format": "{{value}}",
        "min": 0,
        "max": "3.3",
        "colors": [
            "#d70d2f",
            "#d9bb48",
            "#d70d2f"
        ],
        "seg1": "0",
        "seg2": "3.3",
        "x": 1425,
        "y": 125,
        "wires": []
    },
    {
        "id": "cfc96acf.1d89c8",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Add airflow command",
        "func": "msg.payload = \"airflow \" + msg.payload.airflow\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 290,
        "y": 820,
        "wires": [
            [
                "d7f285ba.635ce8"
            ]
        ]
    },
    {
        "id": "d7f285ba.635ce8",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Add Newline",
        "func": "msg.payload = msg.payload+\"\\n\";\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 490,
        "y": 820,
        "wires": [
            [
                "126da37e.25487d",
                "682e8061.c3b05"
            ]
        ]
    },
    {
        "id": "682e8061.c3b05",
        "type": "ui_text",
        "z": "297f7fc8.3b546",
        "group": "6cfbd745.8e4668",
        "order": 3,
        "width": 0,
        "height": 0,
        "name": "Last Sent",
        "label": "Last Sent",
        "format": "{{msg.payload}}",
        "layout": "col-center",
        "x": 735,
        "y": 960,
        "wires": []
    },
    {
        "id": "6c4ed9f1.edcd48",
        "type": "debug",
        "z": "297f7fc8.3b546",
        "name": "Airflow",
        "active": true,
        "tosidebar": true,
        "console": false,
        "tostatus": false,
        "complete": "payload",
        "x": 750,
        "y": 820,
        "wires": []
    },
    {
        "id": "7529d65f.1644d8",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Get Requested Temperature",
        "func": "msg.payload = msg.payload.col8\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 1070,
        "y": 175,
        "wires": [
            [
                "63fdf970.37e688",
                "9796c4ea.f9edd8"
            ]
        ]
    },
    {
        "id": "77fa4f53.f1547",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Get Requested Humidity",
        "func": "msg.payload = msg.payload.col7\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 1060,
        "y": 225,
        "wires": [
            [
                "e4730cae.d8de1"
            ]
        ]
    },
    {
        "id": "a85892ff.91033",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Get Requested Dewpoint",
        "func": "msg.payload = msg.payload.col6\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 1060,
        "y": 275,
        "wires": [
            [
                "7c853989.e01a88"
            ]
        ]
    },
    {
        "id": "63fdf970.37e688",
        "type": "ui_gauge",
        "z": "297f7fc8.3b546",
        "name": "Display Temperature",
        "group": "51c69a15.4dfea4",
        "order": 2,
        "width": "5",
        "height": "4",
        "gtype": "gage",
        "title": "Temperature",
        "label": "°C",
        "format": "{{value}}",
        "min": 0,
        "max": "38",
        "colors": [
            "#45d6d2",
            "#d9bb48",
            "#d70d2f"
        ],
        "seg1": "10",
        "seg2": "32",
        "x": 1310,
        "y": 175,
        "wires": []
    },
    {
        "id": "e4730cae.d8de1",
        "type": "ui_gauge",
        "z": "297f7fc8.3b546",
        "name": "Display Humidity",
        "group": "51c69a15.4dfea4",
        "order": 2,
        "width": "5",
        "height": "4",
        "gtype": "gage",
        "title": "Humidity",
        "label": "%",
        "format": "{{value}}",
        "min": 0,
        "max": "100",
        "colors": [
            "#45d6d2",
            "#d9bb48",
            "#d70d2f"
        ],
        "seg1": "30",
        "seg2": "60",
        "x": 1290,
        "y": 225,
        "wires": []
    },
    {
        "id": "7c853989.e01a88",
        "type": "ui_gauge",
        "z": "297f7fc8.3b546",
        "name": "Display Dewpoint",
        "group": "51c69a15.4dfea4",
        "order": 2,
        "width": "5",
        "height": "4",
        "gtype": "gage",
        "title": "Dewpoint",
        "label": "°C",
        "format": "{{value}}",
        "min": 0,
        "max": "38",
        "colors": [
            "#45d6d2",
            "#d9bb48",
            "#d70d2f"
        ],
        "seg1": "10",
        "seg2": "32",
        "x": 1300,
        "y": 275,
        "wires": []
    },
    {
        "id": "651b15ff.bc245c",
        "type": "ui_form",
        "z": "297f7fc8.3b546",
        "name": "X Form",
        "label": "X Position",
        "group": "c4fedc5c.a4592",
        "order": 1,
        "width": "5",
        "height": "5",
        "options": [
            {
                "label": "",
                "value": "msg",
                "type": "text",
                "required": true
            }
        ],
        "formValue": {
            "msg": ""
        },
        "payload": "",
        "submit": "send",
        "cancel": "cancel",
        "topic": "",
        "x": 50,
        "y": 880,
        "wires": [
            [
                "e0a4e205.842ce"
            ]
        ]
    },
    {
        "id": "1b07df0f.6b3a81",
        "type": "ui_form",
        "z": "297f7fc8.3b546",
        "name": "Y Form",
        "label": "Y Position",
        "group": "c4fedc5c.a4592",
        "order": 2,
        "width": "5",
        "height": "5",
        "options": [
            {
                "label": "",
                "value": "msg",
                "type": "text",
                "required": true
            }
        ],
        "formValue": {
            "msg": ""
        },
        "payload": "",
        "submit": "send",
        "cancel": "cancel",
        "topic": "",
        "x": 50,
        "y": 940,
        "wires": [
            [
                "131db797.4f9478"
            ]
        ]
    },
    {
        "id": "316f20ca.f3cfa",
        "type": "ui_form",
        "z": "297f7fc8.3b546",
        "name": "Z Form",
        "label": "Z Position",
        "group": "c4fedc5c.a4592",
        "order": 3,
        "width": "5",
        "height": "5",
        "options": [
            {
                "label": "",
                "value": "msg",
                "type": "text",
                "required": true
            }
        ],
        "formValue": {
            "msg": ""
        },
        "payload": "",
        "submit": "send",
        "cancel": "cancel",
        "topic": "",
        "x": 50,
        "y": 1000,
        "wires": [
            [
                "eae594bf.b315d8"
            ]
        ]
    },
    {
        "id": "a510815e.e7275",
        "type": "ui_button",
        "z": "297f7fc8.3b546",
        "name": "Zero",
        "group": "c4fedc5c.a4592",
        "order": 5,
        "width": "3",
        "height": "1",
        "passthru": false,
        "label": "Zero",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "icon": "",
        "payload": "zero",
        "payloadType": "str",
        "topic": "",
        "x": 50,
        "y": 1060,
        "wires": [
            [
                "53b12493.f15a1c"
            ]
        ]
    },
    {
        "id": "e0a4e205.842ce",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Add x command",
        "func": "msg.payload = \"PositionX \" + msg.payload.msg\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 290,
        "y": 880,
        "wires": [
            [
                "68e57e43.fcf8a"
            ]
        ]
    },
    {
        "id": "68e57e43.fcf8a",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Add Newline",
        "func": "msg.payload = msg.payload+\"\\n\";\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 490,
        "y": 880,
        "wires": [
            [
                "682e8061.c3b05",
                "126da37e.25487d"
            ]
        ]
    },
    {
        "id": "131db797.4f9478",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Add y command",
        "func": "msg.payload = \"PositionY \" + msg.payload.msg\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 290,
        "y": 940,
        "wires": [
            [
                "e4e76e75.13df1"
            ]
        ]
    },
    {
        "id": "e4e76e75.13df1",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Add Newline",
        "func": "msg.payload = msg.payload+\"\\n\";\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 490,
        "y": 940,
        "wires": [
            [
                "682e8061.c3b05",
                "126da37e.25487d"
            ]
        ]
    },
    {
        "id": "eae594bf.b315d8",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Add z command",
        "func": "msg.payload = \"PositionZ \" + msg.payload.msg\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 290,
        "y": 1000,
        "wires": [
            [
                "fcb05ab8.ef7c28"
            ]
        ]
    },
    {
        "id": "fcb05ab8.ef7c28",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Add Newline",
        "func": "msg.payload = msg.payload+\"\\n\";\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 490,
        "y": 1000,
        "wires": [
            [
                "682e8061.c3b05",
                "126da37e.25487d"
            ]
        ]
    },
    {
        "id": "53b12493.f15a1c",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Add zero command",
        "func": "msg.payload = \"Zero\"\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 290,
        "y": 1060,
        "wires": [
            [
                "78741f5b.233eb"
            ]
        ]
    },
    {
        "id": "78741f5b.233eb",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Add Newline",
        "func": "msg.payload = msg.payload+\"\\n\";\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 490,
        "y": 1060,
        "wires": [
            [
                "682e8061.c3b05",
                "126da37e.25487d"
            ]
        ]
    },
    {
        "id": "eed1338.9fa01d",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Get Actual X",
        "func": "msg.payload = msg.payload.col9;\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 1020,
        "y": 325,
        "wires": [
            [
                "ad8d088.a04f9f8"
            ]
        ]
    },
    {
        "id": "125db5b2.f8bb6a",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Get Actual Y",
        "func": "msg.payload = msg.payload.col10;\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 1020,
        "y": 375,
        "wires": [
            [
                "15081c56.e51034"
            ]
        ]
    },
    {
        "id": "9118397e.4d9e88",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Get Actual Z",
        "func": "msg.payload = msg.payload.col11;\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 1020,
        "y": 425,
        "wires": [
            [
                "f62029e0.7109b8"
            ]
        ]
    },
    {
        "id": "ad8d088.a04f9f8",
        "type": "ui_text",
        "z": "297f7fc8.3b546",
        "group": "c4fedc5c.a4592",
        "order": 11,
        "width": "5",
        "height": "2",
        "name": "X Position",
        "label": "X:",
        "format": "{{msg.payload}}",
        "layout": "row-spread",
        "x": 1280,
        "y": 325,
        "wires": []
    },
    {
        "id": "15081c56.e51034",
        "type": "ui_text",
        "z": "297f7fc8.3b546",
        "group": "c4fedc5c.a4592",
        "order": 12,
        "width": "5",
        "height": "2",
        "name": "Y Position",
        "label": "Y:",
        "format": "{{msg.payload}}",
        "layout": "row-spread",
        "x": 1280,
        "y": 375,
        "wires": []
    },
    {
        "id": "f62029e0.7109b8",
        "type": "ui_text",
        "z": "297f7fc8.3b546",
        "group": "c4fedc5c.a4592",
        "order": 13,
        "width": "5",
        "height": "2",
        "name": "Z Position",
        "label": "Z:",
        "format": "{{msg.payload}}",
        "layout": "row-spread",
        "x": 1280,
        "y": 425,
        "wires": []
    },
    {
        "id": "e5925ae3.0177a8",
        "type": "serial in",
        "z": "297f7fc8.3b546",
        "name": "",
        "serial": "6cfd549c.4d07cc",
        "x": 70,
        "y": 575,
        "wires": [
            [
                "52511212.d42d4c",
                "d8df5f1a.cc349"
            ]
        ]
    },
    {
        "id": "72f1417.115efc",
        "type": "ui_button",
        "z": "297f7fc8.3b546",
        "name": "Velocity",
        "group": "2d3aff8b.56348",
        "order": 17,
        "width": "4",
        "height": "1",
        "passthru": false,
        "label": "Enable Velocity",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "icon": "",
        "payload": "velocity",
        "payloadType": "str",
        "topic": "",
        "x": 60,
        "y": 1125,
        "wires": [
            [
                "2fd52f5d.c829d"
            ]
        ]
    },
    {
        "id": "2fd52f5d.c829d",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Add velocity command",
        "func": "msg.payload = \"Velocity\"\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 285,
        "y": 1125,
        "wires": [
            [
                "4744edf3.23e824"
            ]
        ]
    },
    {
        "id": "4744edf3.23e824",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Add Newline",
        "func": "msg.payload = msg.payload+\"\\n\";\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 495,
        "y": 1125,
        "wires": [
            [
                "682e8061.c3b05",
                "126da37e.25487d"
            ]
        ]
    },
    {
        "id": "1a0b6ea4.ece7d1",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Get Velocity",
        "func": "msg.payload = msg.payload.col16;\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 1020,
        "y": 475,
        "wires": [
            [
                "dceffc77.2443a"
            ]
        ]
    },
    {
        "id": "53f1cc0a.e362c4",
        "type": "ui_gauge",
        "z": "297f7fc8.3b546",
        "name": "Display Velocity",
        "group": "2d3aff8b.56348",
        "order": 12,
        "width": "5",
        "height": "4",
        "gtype": "gage",
        "title": "Sensor Velocity",
        "label": "m/s",
        "format": "{{value}}",
        "min": 0,
        "max": "40",
        "colors": [
            "#d70d2f",
            "#d9bb48",
            "#d70d2f"
        ],
        "seg1": "10",
        "seg2": "35",
        "x": 1380,
        "y": 475,
        "wires": []
    },
    {
        "id": "521c8c0b.b1d6c4",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Get Inside Pressure",
        "func": "msg.payload = msg.payload.col13;\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 1050,
        "y": 525,
        "wires": [
            [
                "51e1f85.6330108"
            ]
        ]
    },
    {
        "id": "96bc90af.adcf",
        "type": "ui_gauge",
        "z": "297f7fc8.3b546",
        "name": "Display Inside Pressure",
        "group": "2d3aff8b.56348",
        "order": 6,
        "width": "5",
        "height": "4",
        "gtype": "gage",
        "title": "Nozzle Pressure",
        "label": "Pa",
        "format": "{{value}}",
        "min": 0,
        "max": "1250",
        "colors": [
            "#d70d2f",
            "#d9bb48",
            "#d70d2f"
        ],
        "seg1": "0",
        "seg2": "1250",
        "x": 1460,
        "y": 525,
        "wires": []
    },
    {
        "id": "6ff0dbc.9e05724",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Get Velocity Pressure",
        "func": "msg.payload = msg.payload.col15;\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 1050,
        "y": 575,
        "wires": [
            [
                "ff41aed7.590f1"
            ]
        ]
    },
    {
        "id": "820a9d91.c29dc",
        "type": "ui_gauge",
        "z": "297f7fc8.3b546",
        "name": "Display Velocity Pressure",
        "group": "2d3aff8b.56348",
        "order": 8,
        "width": "5",
        "height": "4",
        "gtype": "gage",
        "title": "Velocity Pressure",
        "label": "Pa",
        "format": "{{value}}",
        "min": 0,
        "max": "1250",
        "colors": [
            "#d70d2f",
            "#d9bb48",
            "#d70d2f"
        ],
        "seg1": "0",
        "seg2": "1250",
        "x": 1470,
        "y": 575,
        "wires": []
    },
    {
        "id": "da00b4c2.2bbbd8",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Get Outside Pressure",
        "func": "msg.payload = msg.payload.col14;\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 1050,
        "y": 620,
        "wires": [
            [
                "b2adb64.ff2b548"
            ]
        ]
    },
    {
        "id": "b2adb64.ff2b548",
        "type": "ui_gauge",
        "z": "297f7fc8.3b546",
        "name": "Display Outside Pressure",
        "group": "2d3aff8b.56348",
        "order": 7,
        "width": "5",
        "height": "4",
        "gtype": "gage",
        "title": "Pressure Settling Means",
        "label": "Pa",
        "format": "{{value}}",
        "min": 0,
        "max": "750",
        "colors": [
            "#d70d2f",
            "#d9bb48",
            "#d70d2f"
        ],
        "seg1": "0",
        "seg2": "750",
        "x": 1320,
        "y": 620,
        "wires": []
    },
    {
        "id": "9ea05cc1.5e5af",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Get Viscocity",
        "func": "msg.payload = msg.payload.col4\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 1030,
        "y": 675,
        "wires": [
            [
                "608563c4.e88c6c"
            ]
        ]
    },
    {
        "id": "72f9cd1c.380e34",
        "type": "ui_gauge",
        "z": "297f7fc8.3b546",
        "name": "Display Viscocity",
        "group": "51c69a15.4dfea4",
        "order": 2,
        "width": "5",
        "height": "4",
        "gtype": "gage",
        "title": "Air Viscocity",
        "label": "Pa*s",
        "format": "{{value}}E-6",
        "min": "15",
        "max": "20",
        "colors": [
            "#45d6d2",
            "#d9bb48",
            "#d70d2f"
        ],
        "seg1": "17.64",
        "seg2": "18.7",
        "x": 1415,
        "y": 675,
        "wires": []
    },
    {
        "id": "3a41679a.148da8",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Get Fluid Density",
        "func": "msg.payload = msg.payload.col5\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 1040,
        "y": 725,
        "wires": [
            [
                "e6cad70f.928238"
            ]
        ]
    },
    {
        "id": "e6cad70f.928238",
        "type": "ui_gauge",
        "z": "297f7fc8.3b546",
        "name": "Display Air Density",
        "group": "51c69a15.4dfea4",
        "order": 2,
        "width": "5",
        "height": "4",
        "gtype": "gage",
        "title": "Air Density",
        "label": "kg/m^3",
        "format": "{{value}}",
        "min": "0.9",
        "max": "1.7",
        "colors": [
            "#45d6d2",
            "#d9bb48",
            "#d70d2f"
        ],
        "seg1": "1.0",
        "seg2": "1.6",
        "x": 1290,
        "y": 725,
        "wires": []
    },
    {
        "id": "608563c4.e88c6c",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "*10^6",
        "func": "msg.payload = msg.payload*(1000000);\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 1250,
        "y": 675,
        "wires": [
            [
                "72f9cd1c.380e34"
            ]
        ]
    },
    {
        "id": "d8374438.c50528",
        "type": "ui_button",
        "z": "297f7fc8.3b546",
        "name": "Nozzle",
        "group": "2d3aff8b.56348",
        "order": 15,
        "width": "4",
        "height": "1",
        "passthru": false,
        "label": "Enable Nozzle",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "icon": "",
        "payload": "nozzle",
        "payloadType": "str",
        "topic": "",
        "x": 60,
        "y": 1200,
        "wires": [
            [
                "ea83879.e24ba78"
            ]
        ]
    },
    {
        "id": "ea83879.e24ba78",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Add nozzle command",
        "func": "msg.payload = \"Nozzle\"\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 275,
        "y": 1200,
        "wires": [
            [
                "9a936fc0.4655b"
            ]
        ]
    },
    {
        "id": "9a936fc0.4655b",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Add Newline",
        "func": "msg.payload = msg.payload+\"\\n\";\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 495,
        "y": 1200,
        "wires": [
            [
                "126da37e.25487d",
                "682e8061.c3b05"
            ]
        ]
    },
    {
        "id": "48e4fc29.1cc784",
        "type": "switch",
        "z": "297f7fc8.3b546",
        "name": "",
        "property": "payload",
        "propertyType": "msg",
        "rules": [
            {
                "t": "eq",
                "v": " ",
                "vt": "str"
            },
            {
                "t": "neq",
                "v": " ",
                "vt": "str"
            }
        ],
        "checkall": "true",
        "repair": false,
        "outputs": 2,
        "x": 1225,
        "y": 125,
        "wires": [
            [],
            [
                "1ab44c6c.6ad4d4",
                "44289057.a04fe"
            ]
        ]
    },
    {
        "id": "51e1f85.6330108",
        "type": "switch",
        "z": "297f7fc8.3b546",
        "name": "",
        "property": "payload",
        "propertyType": "msg",
        "rules": [
            {
                "t": "eq",
                "v": " ",
                "vt": "str"
            },
            {
                "t": "neq",
                "v": " ",
                "vt": "str"
            }
        ],
        "checkall": "true",
        "repair": false,
        "outputs": 2,
        "x": 1250,
        "y": 525,
        "wires": [
            [],
            [
                "96bc90af.adcf"
            ]
        ]
    },
    {
        "id": "ff41aed7.590f1",
        "type": "switch",
        "z": "297f7fc8.3b546",
        "name": "",
        "property": "payload",
        "propertyType": "msg",
        "rules": [
            {
                "t": "eq",
                "v": " ",
                "vt": "str"
            },
            {
                "t": "neq",
                "v": " ",
                "vt": "str"
            }
        ],
        "checkall": "true",
        "repair": false,
        "outputs": 2,
        "x": 1250,
        "y": 575,
        "wires": [
            [],
            [
                "820a9d91.c29dc"
            ]
        ]
    },
    {
        "id": "dceffc77.2443a",
        "type": "switch",
        "z": "297f7fc8.3b546",
        "name": "",
        "property": "payload",
        "propertyType": "msg",
        "rules": [
            {
                "t": "eq",
                "v": " ",
                "vt": "str"
            },
            {
                "t": "neq",
                "v": " ",
                "vt": "str"
            }
        ],
        "checkall": "true",
        "repair": false,
        "outputs": 2,
        "x": 1200,
        "y": 475,
        "wires": [
            [],
            [
                "53f1cc0a.e362c4",
                "4239a83.d5c0558"
            ]
        ]
    },
    {
        "id": "81a06b52.35fb08",
        "type": "function",
        "z": "297f7fc8.3b546",
        "name": "Get Nozzle Velocity",
        "func": "msg.payload = msg.payload.col17;\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 1050,
        "y": 775,
        "wires": [
            [
                "2a9bc89b.6eed08"
            ]
        ]
    },
    {
        "id": "2a9bc89b.6eed08",
        "type": "switch",
        "z": "297f7fc8.3b546",
        "name": "",
        "property": "payload",
        "propertyType": "msg",
        "rules": [
            {
                "t": "eq",
                "v": " ",
                "vt": "str"
            },
            {
                "t": "neq",
                "v": " ",
                "vt": "str"
            }
        ],
        "checkall": "true",
        "repair": false,
        "outputs": 2,
        "x": 1225,
        "y": 775,
        "wires": [
            [],
            [
                "bbdd2c80.070c7",
                "394889c1.268586"
            ]
        ]
    },
    {
        "id": "bbdd2c80.070c7",
        "type": "ui_gauge",
        "z": "297f7fc8.3b546",
        "name": "Display Nozzle Velocity",
        "group": "2d3aff8b.56348",
        "order": 10,
        "width": "5",
        "height": "4",
        "gtype": "gage",
        "title": "Nozzle Velocity",
        "label": "m/s",
        "format": "{{value}}",
        "min": 0,
        "max": "40",
        "colors": [
            "#d70d2f",
            "#d9bb48",
            "#d70d2f"
        ],
        "seg1": "10",
        "seg2": "35",
        "x": 1410,
        "y": 775,
        "wires": []
    },
    {
        "id": "ca59d3d9.0ced6",
        "type": "ui_chart",
        "z": "297f7fc8.3b546",
        "name": "Velocity",
        "group": "689f4873.2532b8",
        "order": 14,
        "width": 0,
        "height": 0,
        "label": "",
        "chartType": "line",
        "legend": "true",
        "xformat": "HH:mm:ss",
        "interpolate": "linear",
        "nodata": "",
        "dot": false,
        "ymin": "",
        "ymax": "",
        "removeOlder": "5",
        "removeOlderPoints": "300",
        "removeOlderUnit": "60",
        "cutout": 0,
        "useOneColor": false,
        "colors": [
            "#564e83",
            "#d9bb48",
            "#ff7f0e",
            "#2ca02c",
            "#98df8a",
            "#d62728",
            "#ff9896",
            "#9467bd",
            "#c5b0d5"
        ],
        "useOldStyle": false,
        "x": 1960,
        "y": 650,
        "wires": [
            [],
            []
        ]
    },
    {
        "id": "44289057.a04fe",
        "type": "ui_chart",
        "z": "297f7fc8.3b546",
        "name": "Airflow",
        "group": "5b7d2916.9a5e78",
        "order": 13,
        "width": 0,
        "height": 0,
        "label": "",
        "chartType": "line",
        "legend": "false",
        "xformat": "HH:mm:ss",
        "interpolate": "linear",
        "nodata": "",
        "dot": false,
        "ymin": "",
        "ymax": "",
        "removeOlder": "5",
        "removeOlderPoints": "300",
        "removeOlderUnit": "60",
        "cutout": 0,
        "useOneColor": false,
        "colors": [
            "#564e83",
            "#d9bb48",
            "#ff7f0e",
            "#2ca02c",
            "#98df8a",
            "#d62728",
            "#ff9896",
            "#9467bd",
            "#c5b0d5"
        ],
        "useOldStyle": false,
        "x": 1875,
        "y": 125,
        "wires": [
            [],
            []
        ]
    },
    {
        "id": "394889c1.268586",
        "type": "change",
        "z": "297f7fc8.3b546",
        "name": "",
        "rules": [
            {
                "t": "set",
                "p": "topic",
                "pt": "msg",
                "to": "nozzleVelocity",
                "tot": "str"
            }
        ],
        "action": "",
        "property": "",
        "from": "",
        "to": "",
        "reg": false,
        "x": 1905,
        "y": 775,
        "wires": [
            [
                "ca59d3d9.0ced6"
            ]
        ]
    },
    {
        "id": "4239a83.d5c0558",
        "type": "change",
        "z": "297f7fc8.3b546",
        "name": "",
        "rules": [
            {
                "t": "set",
                "p": "topic",
                "pt": "msg",
                "to": "sensorVelocity",
                "tot": "str"
            }
        ],
        "action": "",
        "property": "",
        "from": "",
        "to": "",
        "reg": false,
        "x": 1855,
        "y": 475,
        "wires": [
            [
                "ca59d3d9.0ced6"
            ]
        ]
    },
    {
        "id": "f80cf10.f6dbb1",
        "type": "ui_chart",
        "z": "297f7fc8.3b546",
        "name": "Temperature",
        "group": "edc8336a.c696a",
        "order": 14,
        "width": 0,
        "height": 0,
        "label": "",
        "chartType": "line",
        "legend": "false",
        "xformat": "HH:mm:ss",
        "interpolate": "linear",
        "nodata": "",
        "dot": false,
        "ymin": "",
        "ymax": "",
        "removeOlder": "1",
        "removeOlderPoints": "300",
        "removeOlderUnit": "3600",
        "cutout": 0,
        "useOneColor": false,
        "colors": [
            "#564e83",
            "#d9bb48",
            "#ff7f0e",
            "#2ca02c",
            "#98df8a",
            "#d62728",
            "#ff9896",
            "#9467bd",
            "#c5b0d5"
        ],
        "useOldStyle": false,
        "x": 1920,
        "y": 175,
        "wires": [
            [],
            []
        ]
    },
    {
        "id": "9796c4ea.f9edd8",
        "type": "batch",
        "z": "297f7fc8.3b546",
        "name": "",
        "mode": "interval",
        "count": 10,
        "overlap": 0,
        "interval": "15",
        "allowEmptySequence": false,
        "topics": [],
        "x": 1500,
        "y": 175,
        "wires": [
            [
                "f89fa394.d177a"
            ]
        ]
    },
    {
        "id": "3859fce3.a907f4",
        "type": "switch",
        "z": "297f7fc8.3b546",
        "name": "",
        "property": "parts.index",
        "propertyType": "msg",
        "rules": [
            {
                "t": "eq",
                "v": "1",
                "vt": "str"
            },
            {
                "t": "neq",
                "v": "1",
                "vt": "str"
            }
        ],
        "checkall": "true",
        "repair": false,
        "outputs": 2,
        "x": 1750,
        "y": 175,
        "wires": [
            [
                "f80cf10.f6dbb1"
            ],
            []
        ]
    },
    {
        "id": "f89fa394.d177a",
        "type": "smooth",
        "z": "297f7fc8.3b546",
        "name": "",
        "property": "payload",
        "action": "mean",
        "count": "15",
        "round": "",
        "mult": "single",
        "x": 1625,
        "y": 175,
        "wires": [
            [
                "3859fce3.a907f4"
            ]
        ]
    }
]