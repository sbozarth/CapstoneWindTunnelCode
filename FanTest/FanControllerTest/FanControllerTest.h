#ifndef FAN_CONTROL_FILE
#define FAN_CONTROL_FILE

/****************************/
/* Timer 1 type definitions */
/****************************/

typedef enum{
	WGM1_NORMAL = 0x0,
	WGM1_PWM_PHASE_CORRECT_8 = 0x1,
	WGM1_PWM_PHASE_CORRECT_9 = 0x2,
	WGM1_PWN_PHASE_CORRECT_10 = 0x3,
	WGM1_CTC_OCR1A = 0x4,
	WGM1_FAST_PWM_8 = 0x5,
	WGM1_FAST_PWM_9 = 0x6,
	WGM1_FAST_PWM_10 = 0x7,
	WGM1_PWM_FREQ_ICR1 = 0x8,
	WGM1_PWM_FREQ_OCR1A = 0x9,
	WGM1_PWM_PHASE_ICR1 = 0xA,
	WGM1_PWM_PHASE_OCR1A = 0xB,
	WGM1_PWM_CTC_ICR1 = 0xC,
}wgm1_t;

typedef enum{
	COM1_NORMAL = 0x0,
	COM1_TOGGLE = 0x1,
	COM1_CLEAR = 0x2,
	COM1_SET = 0x3,
}com1_t;

typedef enum{
	CS1_NO_CLOCK = 0x0,
	CS1_PRESCALE_1 = 0x1,
	CS1_PRESCALE_8 = 0x2,
	CS1_PRESCALE_64 = 0x3,
	CS1_PRESCALE_256 = 0x4,
	CS1_PRESCALE_1024 = 0x5,
	CS1_EXTERNAL_FALLING = 0x6,
	CS1_EXTERNAL_RISING = 0x7,
}cs1_t;

typedef enum{
	T1INTERUPT_DISABLE = 0x0,
	T1INTERUPT_ENABLE = 0x1,
}t1interupt_t;
#endif
