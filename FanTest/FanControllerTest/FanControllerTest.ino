/*
 * FanControllerTest.c
 *
 * Created: 11/29/2018 6:55:09 PM
 * Author : sbozarth
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>

#include <stdint.h>

#include "FanControllerTest.h"


void init_timer1(wgm1_t wgm1, com1_t com1a, com1_t com1b, cs1_t cs1, t1interupt_t t1interupt, uint8_t ocr1a, uint8_t ocr1b){
  
  TCCR1A = ((com1a&0x3)<<COM1A0) | ((com1b&0x3)<<COM1B0) | ((wgm1&0x3)<<WGM10);
  
  TCCR1B = ((wgm1&0xC)<<WGM12>>2) | ((cs1&0x7)<<CS10);
  
  OCR1A = ocr1a;
  
  OCR1B = ocr1b;
  
  TIMSK1 = (1<<OCIE1A);// | (1<<OCIE1B);
}


/* Each tick is 20 kHz */
/* 9000 for ~  8.55 hz */
/* 8500 for ~  9.05 hz */
/* 8000 for ~  9.62 hz */
/* 7500 for ~ 10.23 hz */
/* 7000 for ~ 11.00 hz */
/* 6500 for ~ 11.84 hz */
/* 6000 for ~ 12.82 hz */
/* 5500 for ~ 14.00 hz */
/* 5000 for ~ 15.39 hz */
/* 4500 for ~ 17.10 hz */
/* 4000 for ~ 19.24 hz */
/* 3500 for ~ 21.98 hz */
/* 3000 for ~ 25.65 hz */
/* 2500 for ~ 30.78 hz */
/* 2000 for ~ 38.49 hz */
/* 1500 for ~ 51.27 hz */
/* 1300 for ~ 59.20 hz */
/* 1283 for ~ 59.96 hz */
/* 1282 for ~ 60.02 hz */
/* 1275 for ~ 60.36 hz */
/* 1250 for ~ 61.57 hz */
/* 1000 for ~ 76.95 hz */
/* Hz = 77028/tick     */

uint32_t ammount = 0;
uint32_t overflowValue = 1750;
uint32_t overflowHalfway = overflowValue/2;

ISR (TIMER1_COMPA_vect)
{
  ++ammount;
  //PORTC ^= (1<<PORTC1);
  
  if (ammount >= overflowValue){
    PINC= (1<<PORTC1);
    ammount = 0;
  }
  else if (ammount == overflowValue-150){
    PINC = (1<<PORTC2);
  }
  else if (ammount == overflowHalfway-150){
    PINC = (1<<PORTC1);
  }
  else if (ammount == overflowHalfway){
    PINC = (1<<PORTC2);
  }
  
}

const byte numChars = 16;
char receivedChars[numChars]; // an array to store the received data

boolean newData = false;

void recvWithEndMarker() {
  static byte ndx = 0;
  char endMarker = '\n';
  char rc;
 
  if (Serial.available() > 0) {
    while (Serial.available() > 0 && newData == false) {
      rc = Serial.read();
      if (rc != endMarker) {
        //Serial.println("!");
        receivedChars[ndx] = rc;
        ndx++;
        if (ndx >= numChars) {
          ndx = numChars - 1;
        }
      }
      else {
        receivedChars[ndx] = '\0'; // terminate the string
        ndx = 0;
        newData = true;
      }
    }
  }
}

void setup()
{
  Serial.begin(9600);
  
  init_timer1(WGM1_CTC_OCR1A,COM1_TOGGLE,COM1_NORMAL,CS1_PRESCALE_8,T1INTERUPT_ENABLE,25,25);
  DDRC|= (1<<PORTC2) | (1<<PORTC1);
  DDRC |= (1<<PORTC0);
  DDRB |= (1<<PORTB5);
  PINC= (1<<PORTC1);
  sei();
}

void loop() {
    /* Replace with your application code */
    // READ DATA
  recvWithEndMarker();

  delay(1000);
}
