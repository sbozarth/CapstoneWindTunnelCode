#include "timer1.h"
#include <avr/io.h> 

/************************************************************************/
/* Name: Daniel Scott Bozarth                                           */
/* Date: 2018-09-26                                                     */
/*                                                                      */
/* Hardware Requirements:                                               */
/* ATMega328P                                                           */
/*                                                                      */
/* Build Requirements:                                                  */
/* avr/io library is needed                                             */
/* Built using the following commands                                   */
/* make                                                                 */
/*                                                                      */
/* Modification History:                                                */
/* Initial revision (2018-09-26):                                       */
/*         revision (2018-10-11): Made interrupt programmable           */
/*         revision (2018-11-28): Fixed name collision with timer0      */
/************************************************************************/

void init_timer1(wgm1_t wgm1, com1_t com1a, com1_t com1b, cs1_t cs1, t1interupt_t t1interupt, uint8_t ocr1a, uint8_t ocr1b){
	
	TCCR1A = ((com1a&0x3)<<COM1A0) | ((com1b&0x3)<<COM1B0) | ((wgm1&0x3)<<WGM10);
	
	TCCR1B = ((wgm1&0xC)<<WGM12>>2) | ((cs1&0x7)<<CS10);
	
	OCR1A = ocr1a;
	
	OCR1B = ocr1b;
	
	TIMSK1 = (1<<OCIE1A);// | (1<<OCIE1B);
}