/*
 * FanControllerTest.c
 *
 * Created: 11/29/2018 6:55:09 PM
 * Author : sbozarth
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>

#include "timer1.h"


/* Each tick is 20 kHz*/
/* 333 for ~ 60 hz */
/* 571 for ~ 35 hz */
/* 2000 for ~ 10 hz */
uint32_t ammount = 0;
uint32_t overflowValue = 2000;
uint32_t overflowHalfway = 1000;

ISR (TIMER1_COMPA_vect){
	++ammount;
	if (ammount >= overflowValue){
		ammount = 0;
		//PORTB ^= (1<<PORTB1);
	}
	else if (ammount == overflowValue-1){
		//PORTB &= ~(1<<PORTB2);
	}
	else if (ammount == overflowHalfway-1){
		//PORTB &= ~(1<<PORTB1);
	}
	else if (ammount == overflowHalfway){
		//PORTB |= (1<<PORTB2);
	}
	
}

void setup()
{
	init_timer1(WGM1_CTC_OCR1A,COM1_NORMAL,COM1_NORMAL,CS1_PRESCALE_8,T1INTERUPT_ENABLE,250,250);
	DDRB |= (1<<PORTB2) | (1<<PORTB1);
	//PORTB ^= (1<<PORTB0);
	sei();
}

int main(void)
{
	setup();
	DDRB |= (1<<PORTB0);
    /* Replace with your application code */
    while (1){
		PORTB ^= (1<<PORTB0);
    }
}

