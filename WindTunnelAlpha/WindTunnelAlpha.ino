/**************************************************************/
/* Wind Tunnel Arduino Code                                   */
/* Arduino MEGA 2560                                          */
/*                                                            */
/* WARNING: If you are running this code on another computer  */
/* you MUST go into the Servo.h file at                       */
/* /usr/share/arduino/libraries/Servo on linux or on linux    */
/* and comment out the lines that give the servo library      */
/* control over Timer3 and timer4 for the 2560 processor      */
/*                                                            */
/* Realisitcaly we need to wait for about a second after each */
/* measurement to wait for it to settle, so speed from        */
/* division isnt an issue. Need to do a comparison between    */
/* accum and double for percesion, but the loss from doubles  */
/* is negligible compared to the loss from sensors and        */
/* measuring percesion.                                       */
/**************************************************************/

#include <avr/io.h>
#include <avr/interrupt.h>

#include <stdint.h>
#include <math.h>

#include "WindTunnelAlpha.h"

#include <Adafruit_Sensor.h>

#include <Servo.h>

#include <Wire.h>
#include <Adafruit_BMP085_U.h>
/* REPORTS IN hPa not Pa */

#define DHT22_PIN 56
#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
#include <DHT_U.h>
#include <DHT.h>
DHT dht(DHT22_PIN, DHTTYPE);

#define DELAY_TIME_COM 1000
#define DELAY_TIME_LOG 1000
#define GAS_CONSTANT 287.1 /*J/(kg*K) */

/* Variable declerations */
double currentFluidDensity;
double currentViscosity;
double currentY;
double currentCd = 1;
double currentQ;
double currentQminute;
double currentDewpoint;
double currentTemperature = 21;
double currentHumidity = 0.5;

double nozzleVelocity = 0;

double diameterMeas;
#define CHAMBER_DIAMETER 0.4 /* 40 cm */

#define NOZZLE_SMALL_DIAMETER 0.01464
#define NOZZLE_MEDIUM_DIAMETER 0.02132
#define NOZZLE_LARGE_DIAMETER 0.03766
#define NOZZLE_SMALL_AREA 0.000168334
#define NOZZLE_MEDIUM_AREA 0.000356997
#define NOZZLE_LARGE_AREA 0.00111391
boolean nozzleSmallOpen = false;
boolean nozzleMediumOpen= false;
boolean nozzleLargeOpen = true;
double nozzleDiameter = NOZZLE_LARGE_DIAMETER;
double nozzleArea = NOZZLE_LARGE_AREA;

#define HERTZ_MIN 12
#define HERTZ_MAX 60

int noPanic = 1;

ISR (INT5_vect){
  #if 0
  while(1){ /* panic shutdown */
    PORTC &= 0;
  }
  #endif
  noPanic = 1;
}

void debugTag(){
  Serial.print("DEBUG:");
  Serial.print(millis()/1000.0,3);
  Serial.print(": ");
}

void logTag(){
  Serial.print("LOG:");
  Serial.print(millis()/1000.0,3);
  Serial.print(", ");
}

void init_timer3(wgm3_t wgm3, com3_t com3a, com3_t com3b, cs3_t cs3, t3interupt_t t3interupt, uint8_t ocr3a, uint8_t ocr3b){
  TCCR3A = ((com3a&0x3)<<COM3A0) | ((com3b&0x3)<<COM3B0) | ((wgm3&0x3)<<WGM30);
  
  TCCR3B = ((wgm3&0xC)<<WGM32>>2) | ((cs3&0x7)<<CS30);
  
  OCR3A = ocr3a;
  
  OCR3B = ocr3b;
  
  TIMSK3 = (1<<OCIE1B);//(1<<OCIE3A);
}
void init_timer4(wgm4_t wgm4, com4_t com4a, com4_t com4b, cs4_t cs4, t4interupt_t t4interupt, uint8_t ocr4a, uint8_t ocr4b){
  TCCR4A = ((com4a&0x3)<<COM4A0) | ((com4b&0x3)<<COM4B0) | ((wgm4&0x3)<<WGM40);
  
  TCCR4B = ((wgm4&0xC)<<WGM42>>2) | ((cs4&0x7)<<CS40);
  
  OCR4A = ocr4a;
  
  OCR4B = ocr4b;
  
  TIMSK4 = (1<<OCIE1B);//(1<<OCIE3A);
}


#define SMALL_CAP_PIN 8
#define MEDIUM_CAP_PIN 9
#define LARGE_CAP_PIN 10

int ammount = 0;
#define OVERFLOW_MAX 7677
#define OVERFLOW_MIN 1279
#define OVERFLOW_MIDDLE 2559
int overflowValue = OVERFLOW_MIDDLE;
int overflowHalfway = overflowValue>>1;
int requestedOverflowValue = overflowValue;
int deadband = overflowValue/5;

int tickAmmount = 0;
int maxTicks = 639;
int requestedTicks = 500;
int currentRequestedTicks = requestedTicks;
ISR (TIMER3_COMPB_vect)
{
  ++ammount;
  if (ammount >= overflowValue){
    PINC = (1<<PORTC1);
    ammount = 0;
    if (overflowValue != requestedOverflowValue){
      overflowValue = requestedOverflowValue;
      overflowHalfway = overflowValue>>1;
      deadband = overflowValue/5;
    }
  }
  else if (ammount == overflowValue-deadband){
    PINC = (1<<PORTC2);
  }
  else if (ammount == overflowHalfway-deadband){
    PINC = (1<<PORTC1);
  }
  else if (ammount == overflowHalfway){
    PINC = (1<<PORTC2);
  }
  
  ++tickAmmount;
  if (tickAmmount == (maxTicks-currentRequestedTicks)){
    currentRequestedTicks = requestedTicks;
    PORTC |= (1<<PORTC6);
  }
  if(tickAmmount == maxTicks){
    PORTC &= ~(1<<PORTC6);
  }
  
}

ISR (INT4_vect){
  PORTC &= ~(1<<PORTC6);
  tickAmmount = 0;
  currentRequestedTicks = requestedTicks;
}

Servo smallServo;  // create servo object to control a servo
Servo mediumServo;
Servo largeServo;

#define SMALL_MAX_POS 157
#define SMALL_MIN_POS 49
int smallPos = SMALL_MAX_POS;

#define MEDIUM_MAX_POS 170
#define MEDIUM_MIN_POS 62
int mediumPos = MEDIUM_MAX_POS;

#define LARGE_MAX_POS 114
#define LARGE_MIN_POS 6
int largePos = LARGE_MAX_POS;

int servoOpen(int currentPos, int minPos, int maxPos, Servo servo){
  for (currentPos; currentPos >= minPos; currentPos -= 1) { // goes from 180 degrees to 0 degrees
    servo.write(currentPos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }
  return currentPos;
}

int servoClose(int currentPos, int minPos, int maxPos, Servo servo){
  for (currentPos; currentPos <= maxPos; currentPos += 1) { // goes from 180 degrees to 0 degrees
    servo.write(currentPos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }
  return currentPos;
}

double calculateTempCd(double Re){
  return 0.9986 - 7.006/sqrt(Re) + 134.6/Re;
}

double sqrtFourABoverPi(double diameterA, double diameterB){
  return sqrt((1.2732395*diameterA*diameterB)); /* float is pi/4 */
}

/* nozzleDiameter/diameterMeas ^ 4 can be calculated, and passed in when the nozzles are open */
double calculateY(float nozzleDiameter, double pressureChange, double fluidDensity, double measTemperature){
  /* double temp = (nozzleDiameter/diameterMeas); */
  return 1-(0.548)*(1-(1-pressureChange/(fluidDensity*GAS_CONSTANT*(measTemperature+273.15))));
}

/* sqrt(2)/viscocity  could be pre calculated and passed in, upated only when viscocity changes */
double calculateRe(double Cd, double viscocity, float nozzleDiameter, double Y, double pressureChange, double fluidDensity){
  return (1.41421356/viscocity)*Cd*nozzleDiameter*Y*sqrt(pressureChange*fluidDensity); /* float is sqrt(2) */
}

double calculateCd(double Cd, double viscocity, float nozzleDiameter, double Y, double pressureChange, double fluidDensity, double measTemperature){
  int failcount = 0;
  double oldCd = Cd;
  #ifdef DEBUG_TAG
  debugTag();
  Serial.print("Initial Cd: ");
  Serial.println(oldCd,6);
  #endif
  //Serial.println("RE");
  double Re = calculateRe(Cd,viscocity,nozzleDiameter,Y,pressureChange,fluidDensity);
  #ifdef DEBUG_TAG
  debugTag();
  Serial.print("Initial Re: ");
  Serial.println(Re,6);
  #endif
  //Serial.println("newCD");
  double newCd = calculateTempCd(Re);
  #ifdef DEBUG_TAG
  debugTag();
  Serial.print("Temp Cd: ");
  Serial.println(newCd,6);
  #endif
  //Serial.println("CDchange");
  double cdChange = oldCd-newCd;
  //Serial.println(cdChange,6);
  while (!((cdChange < 0.001) && (cdChange > -0.001))){
    ++failcount;
    oldCd = newCd;
    Re = calculateRe(Cd,viscocity,nozzleDiameter,Y,pressureChange,fluidDensity);
    newCd = calculateTempCd(Re);
    cdChange = oldCd-newCd;
    #ifdef DEBUG_TAG
    debugTag();
    Serial.println(cdChange,8);
    #endif
    if (failcount > 1000){
      #ifdef DEBUG_TAG
      debugTag();
      Serial.println("CD failed");
      #endif
      return Cd;
    }
  }
  #ifdef DEBUG_TAG
  debugTag();
  Serial.print("Final Cd: ");
  Serial.println(newCd);
  #endif
  return newCd;
}

/* nozzleDiameter/chamber diameter can be pre calculated and passed in when the open nozzles are set */
double calculateQ(double Cd, double Y, double nozzleArea, double pressureChange, double fluidDensity){
  //debugTag();
  //Serial.print("Temp: ");
  //Serial.print(temp);
  //Serial.print(" CD: ");
  //Serial.print(Cd,6);
  //Serial.print(" Y: ");
  //Serial.print(Y,6);
  //Serial.print(" Diameter: ");
  //Serial.print(nozzleDiameter, 4);
  //Serial.print(" Change: ");
  //Serial.print(pressureChange);
  //Serial.print(" Fluid Density: ");
  //Serial.print(fluidDensity);
  return Cd*Y*1.414213562*nozzleArea*sqrt((pressureChange)/(fluidDensity));
}

/* Lookup table or equation */
#define FLUID_DENSITY_DEFAULT 1.2
double findFluidDensity(double measTemperature,double pressure,double dewpoint){
  double fluidDensity = FLUID_DENSITY_DEFAULT; /* kg/m^3 at room temp */
  return fluidDensity;
}

/* Lookup table or equation */
#define VISCOCITY_DEFAULT 0.00001821 
double findViscocity(double measTemperature,double pressure){
  double viscosity = VISCOCITY_DEFAULT; /* kg/(m*s) at room temp */
  return viscosity;
}

#define DEWPOINT_ROOM_TEMPERATURE 6
double findDewpoint(double measTemperature, double measHumidity){
  /* (simple) aproximation for dewpoint assuming we start with nominal MAKE BETTER */
  double dewpoint = DEWPOINT_ROOM_TEMPERATURE; /* Aproximate dewpoint in celcius */
  double RH = 100-5*(measTemperature-dewpoint);
  dewpoint = measTemperature-(100-RH)/5;
  return dewpoint;
}

const byte numChars = 32;
char receivedChars[numChars]; // an array to store the received data
boolean newData = false;

void recvWithEndMarker() {
  static byte ndx = 0;
  char endMarker = '\n';
  char rc;
 
  if (Serial.available() > 0) {
    while (Serial.available() > 0 && newData == false) {
      rc = Serial.read();
      if (rc != endMarker) {
        //Serial.println("!");
        receivedChars[ndx] = rc;
        ndx++;
        if (ndx >= numChars) {
          ndx = numChars - 1;
        }
      }
      else {
        receivedChars[ndx] = '\0'; // terminate the string
        ndx = 0;
        newData = true;
        
      }
    }
  }
}

/* 248.8271 Conversion factor from dwyer website blog post, google sugests conversion factor is 248.84*/
#define INWC_TO_PASCAL 248.8271

#define PRESSURE_PIN_INSIDE A0
#define PRESSURE_PIN_OUTSIDE A1
#define ANALOG_MEASURMENT_RANGE 1024.0
#define INSIDEPRESSURE_INWC_RANGE 5.0
#define INSIDEPRESSURE_PASCAL_RANGE INSIDEPRESSURE_INWC_RANGE*INWC_TO_PASCAL
#define OUTSIDEPRESSURE_INWC_RANGE 3.0
#define OUTSIDEPRESSURE_PASCAL_RANGE OUTSIDEPRESSURE_INWC_RANGE*INWC_TO_PASCAL
double currentInsidePressureMeas = 0;
double currentVelocityPressureMeas = 0;
double currentVelocity = 0;
double currentOutsidePressureMeas = 0;
double currentPressureDifferenceMeas;

double artopa(int measurment, double range){ /* analog pressure measurement reading to actual */
  return ((double)measurment/ANALOG_MEASURMENT_RANGE)*range*(5.0384/5);
}

void updatePressure(){
  currentInsidePressureMeas = artopa(analogRead(PRESSURE_PIN_INSIDE), INSIDEPRESSURE_PASCAL_RANGE );
  #ifdef DEBUG_TAG
  debugTag();
  Serial.print("Inside pressure ");
  Serial.print(analogRead(PRESSURE_PIN_INSIDE));
  Serial.print(" /1024, \n");
  #endif
  currentOutsidePressureMeas = artopa(analogRead(PRESSURE_PIN_OUTSIDE), OUTSIDEPRESSURE_PASCAL_RANGE );
  #ifdef DEBUG_TAG
  debugTag();
  Serial.print("Outside pressure ");
  Serial.print(currentOutsidePressureMeas);
  Serial.print(" /1024, \n");
  #endif
  currentPressureDifferenceMeas = currentOutsidePressureMeas - currentInsidePressureMeas;
  if (currentPressureDifferenceMeas < 0){
    currentPressureDifferenceMeas = currentPressureDifferenceMeas *-1;
    Serial.println("Error(updatePressure): inside pressure measured greater than outside pressure");  
  }
  if (currentPressureDifferenceMeas == 0){
    currentPressureDifferenceMeas = 0.0000001; 
  }
}

#define PRESSURE_SWITCH_PIN 7

void updateVelocity(){ /* Need to switch pressure_switch_pin, but doing that outside for better timing */
  int temp = analogRead(PRESSURE_PIN_INSIDE);
  #ifdef DEBUG_TAG
  debugTag();
  Serial.print("Range: ");
  Serial.println(INSIDEPRESSURE_PASCAL_RANGE);
  #endif
  currentVelocityPressureMeas = artopa(temp, INSIDEPRESSURE_PASCAL_RANGE);
  #ifdef DEBUG_TAG
  debugTag();
  Serial.print("Velocity Inside pressure ");
  Serial.print(temp);
  Serial.print(" /1024, \n");
  #endif
  currentVelocity = sqrt((currentVelocityPressureMeas*2)/(currentFluidDensity));
  currentOutsidePressureMeas = artopa(analogRead(PRESSURE_PIN_OUTSIDE), OUTSIDEPRESSURE_PASCAL_RANGE );
  #ifdef DEBUG_TAG
  debugTag();
  Serial.print("Outside pressure ");
  Serial.print(currentOutsidePressureMeas);
  Serial.print(" /1024, \n");
  #endif
}

#define TICK_CONVERSION_FACTOR 76767
#define DEADBAND_CONVERSION_FACTOR 15353

double htotr(double requestedHertz){
  double requestedHertzSquare = requestedHertz * requestedHertz;
  double requestedHertzCube = requestedHertzSquare * requestedHertz;
  return (0.0000048*requestedHertzCube-0.000317297*requestedHertzSquare+0.010164001*requestedHertz)*maxTicks;
}

double ttoh(int tick){
  /* For 20kHz tick */
  return (double)TICK_CONVERSION_FACTOR/tick;
}

int htot(double hertz){
  /* For 20kHz tick */
  return round(TICK_CONVERSION_FACTOR/hertz);
}

int htodb(double hertz){
  return round(DEADBAND_CONVERSION_FACTOR/hertz);
}

int ttodb(int tick){
  return tick/5;  
}

int cmtot(float cm){
  return round(cm/0.004);
}

double ttocm(int tick){
  return tick*0.004;
}

#define MOTOR_X_ENABLE_PIN 25
#define MOTOR_X_STEP_PIN 27
#define MOTOR_X_DIRECTION_PIN 29
#define MOTOR_X_ZERO_SWITCH 23

#define MOTOR_Y_ENABLE_PIN 30
#define MOTOR_Y_STEP_PIN 26
#define MOTOR_Y_DIRECTION_PIN 24
#define MOTOR_Y_ZERO_SWITCH 22

#define MOTOR_Z_ENABLE_PIN 38
#define MOTOR_Z_STEP_PIN 39
#define MOTOR_Z_DIRECTION_PIN 40
#define MOTOR_Z_ZERO_SWITCH 41

#define XSM1 45
#define XSM2 47
#define YSM1 44
#define YSM2 46
#define ZSM1 42
#define ZSM2 43

int locationX = 0;
//int requestedPositionX = 0;
int locationY = 0;
//int requestedPositionY = 0;
int locationZ = 0;
//int requestedPositionZ = 0;
int timeMovingCom = 0;
int timeMovingLog = 0;

#define STEP_WAIT 1
void motorXmove(int requestedPositionX){
  #ifdef DEBUG_TAG
  debugTag();
  Serial.print("X Ticks ");
  Serial.println(requestedPositionX);
  #endif
  if (locationX == requestedPositionX){
        
  }
  else if (locationX < requestedPositionX){
    digitalWrite(MOTOR_X_DIRECTION_PIN,LOW);
    Serial.println("MOTOR_DIRECTION TOWARD ZERO");
    for(locationX; locationX < requestedPositionX; ++locationX) // Loop 200 times
    {
      digitalWrite(MOTOR_X_STEP_PIN,HIGH); // Output high
      delay(STEP_WAIT); // Wait
      digitalWrite(MOTOR_X_STEP_PIN,LOW); // Output low
      delay(STEP_WAIT); // Wait
      timeMovingCom +=2*STEP_WAIT;
      timeMovingLog = timeMovingCom;
    }
  }
  else if (locationX > requestedPositionX){
    Serial.println("MOTOR_DIRECTION AWAY ZER0");
    digitalWrite(MOTOR_X_DIRECTION_PIN,HIGH);
    for(locationX; locationX > requestedPositionX; --locationX) // Loop 200 times
    {
      digitalWrite(MOTOR_X_STEP_PIN,HIGH); // Output high
      delay(STEP_WAIT); // Wait
      digitalWrite(MOTOR_X_STEP_PIN,LOW); // Output low
      delay(STEP_WAIT); // Wait
      timeMovingCom +=2*STEP_WAIT;
      timeMovingLog = timeMovingCom;
    }
  }
}

void motorYmove(int requestedPositionY){
  #ifdef DEBUG_TAG
  debugTag();
  Serial.print("Y Ticks ");
  Serial.println(requestedPositionY);
  #endif
  if (locationY == requestedPositionY){
        
  }
  else if (locationY < requestedPositionY){
    digitalWrite(MOTOR_Y_DIRECTION_PIN,LOW);
    Serial.println("MOTOR_DIRECTION UP");
    for(locationY; locationY < requestedPositionY; ++locationY) // Loop 200 times
    {
      digitalWrite(MOTOR_Y_STEP_PIN,HIGH); // Output high
      delay(STEP_WAIT); // Wait
      digitalWrite(MOTOR_Y_STEP_PIN,LOW); // Output low
      delay(STEP_WAIT); // Wait
      timeMovingCom +=2*STEP_WAIT;
      timeMovingLog = timeMovingCom;
    }
  }
  else if (locationY > requestedPositionY){
    Serial.println("MOTOR_DIRECTION DOWN");
    digitalWrite(MOTOR_Y_DIRECTION_PIN,HIGH);
    for(locationY; locationY > requestedPositionY; --locationY) // Loop 200 times
    {
      digitalWrite(MOTOR_Y_STEP_PIN,HIGH); // Output high
      delay(STEP_WAIT); // Wait
      digitalWrite(MOTOR_Y_STEP_PIN,LOW); // Output low
      delay(STEP_WAIT); // Wait
      timeMovingCom +=2*STEP_WAIT;
      timeMovingLog = timeMovingCom;
    }
  }
}

void motorZmove(int requestedPositionZ){
  #ifdef DEBUG_TAG
  debugTag();
  Serial.print("Z Ticks ");
  Serial.println(requestedPositionZ);
  #endif
  if (locationZ == requestedPositionZ){
        
  }
  else if (locationZ < requestedPositionZ){
    digitalWrite(MOTOR_Z_DIRECTION_PIN,LOW);
    Serial.println("MOTOR_DIRECTION LOW");
    for(locationZ; locationZ < requestedPositionZ; ++locationZ) // Loop 200 times
    {
      digitalWrite(MOTOR_Z_STEP_PIN,HIGH); // Output high
      delay(STEP_WAIT); // Wait
      digitalWrite(MOTOR_Z_STEP_PIN,LOW); // Output low
      delay(STEP_WAIT); // Wait
      timeMovingCom +=2*STEP_WAIT;
      timeMovingLog = timeMovingCom;
    }
  }
  else if (locationZ > requestedPositionZ){
    Serial.println("MOTOR_DIRECTION HIGH");
    digitalWrite(MOTOR_Z_DIRECTION_PIN,HIGH);
    for(locationZ; locationZ > requestedPositionZ; --locationZ) // Loop 200 times
    {
      digitalWrite(MOTOR_Z_STEP_PIN,HIGH); // Output high
      delay(STEP_WAIT); // Wait
      digitalWrite(MOTOR_Z_STEP_PIN,LOW); // Output low
      delay(STEP_WAIT); // Wait
      timeMovingCom +=2*STEP_WAIT;
      timeMovingLog = timeMovingCom;
    }
  }
}

void motorAllMove(int requestedPositionX, int requestedPositionY, int requestedPositionZ){
  motorXmove(requestedPositionX);
  motorYmove(requestedPositionY);
  motorZmove(requestedPositionZ);
}

#define Z_SAFE 0
#define X_SAFE 10
#define Y_SAFE 10

#define ZERO_OFFSET 50

void motorAllSafe(){
  motorZzero();
  motorXmove(X_SAFE);
  motorYmove(Y_SAFE);
}

void motorXzero(){
  #ifdef DEBUG_TAG
  debugTag();
  Serial.println("Zero X");
  #endif
  digitalWrite(MOTOR_X_DIRECTION_PIN,HIGH); // Set Dir high
  while (digitalRead(MOTOR_X_ZERO_SWITCH) == LOW){
    digitalWrite(MOTOR_X_STEP_PIN,HIGH); // Output high
    delay(1); // Wait
    digitalWrite(MOTOR_X_STEP_PIN,LOW); // Output low
    delay(1); // Wait
  }
  digitalWrite(MOTOR_X_DIRECTION_PIN,LOW); // Set Dir Low
  for(int i = 0; i < ZERO_OFFSET;++i){
    digitalWrite(MOTOR_X_STEP_PIN,HIGH); // Output high
    delay(1); // Wait
    digitalWrite(MOTOR_X_STEP_PIN,LOW); // Output low
    delay(1); // Wait
  }
  locationX = 0;
}

void motorYzero(){
  #ifdef DEBUG_TAG
  debugTag();
  Serial.println("Zero Y");
  #endif
  digitalWrite(MOTOR_Y_DIRECTION_PIN,HIGH); // Set Dir high
  while (digitalRead(MOTOR_Y_ZERO_SWITCH) == LOW){
    digitalWrite(MOTOR_Y_STEP_PIN,HIGH); // Output high
    delay(1); // Wait
    digitalWrite(MOTOR_Y_STEP_PIN,LOW); // Output low
    delay(1); // Wait
  }
  digitalWrite(MOTOR_Y_DIRECTION_PIN,LOW); // Set Dir Low
  for(int i = 0; i < ZERO_OFFSET;++i){
    digitalWrite(MOTOR_Y_STEP_PIN,HIGH); // Output high
    delay(1); // Wait
    digitalWrite(MOTOR_Y_STEP_PIN,LOW); // Output low
    delay(1); // Wait
  }
  locationY = 0;
}

void motorZzero(){
  #ifdef DEBUG_TAG
  debugTag();
  Serial.println("Zero Z");
  #endif
  digitalWrite(MOTOR_Z_DIRECTION_PIN,HIGH); // Set Dir high
  while (digitalRead(MOTOR_Z_ZERO_SWITCH) == LOW){
    digitalWrite(MOTOR_Z_STEP_PIN,HIGH); // Output high
    delay(1); // Wait
    digitalWrite(MOTOR_Z_STEP_PIN,LOW); // Output low
    delay(1); // Wait
  }
  digitalWrite(MOTOR_Z_DIRECTION_PIN,LOW); // Set Dir Low
  for(int i = 0; i < ZERO_OFFSET;++i){
    digitalWrite(MOTOR_Z_STEP_PIN,HIGH); // Output high
    delay(1); // Wait
    digitalWrite(MOTOR_Z_STEP_PIN,LOW); // Output low
    delay(1); // Wait
  }
  locationZ = 0;
}

void motorAllZero(){
  motorZzero();
  motorXzero();
  motorYzero();
}

double absoluteDifference = 0;
int increment = 0;
double incrementHertz = 1;
double requestedQ = 0;
char delimiter[3] = ", ";
int failHigh = 0;
int failLow = 0;
#define FAIL_THRESHOLD 100
unsigned long long loopCountCommunication = 0;
unsigned long long loopCountLogic = 0;
boolean switchToggle = false;

void setup() {
  Serial.begin(115200);
  Serial1.begin(115200);
  debugTag();
  Serial.println("Begin Setup");
  analogReference(EXTERNAL);
  analogRead (0);
  pinMode(2, INPUT);
  pinMode(30, OUTPUT);
  pinMode(PRESSURE_SWITCH_PIN, OUTPUT);
  pinMode(MOTOR_X_ENABLE_PIN,OUTPUT); // Enable
  pinMode(MOTOR_X_STEP_PIN,OUTPUT); // Step
  pinMode(MOTOR_X_DIRECTION_PIN,OUTPUT); // Dir
  pinMode(MOTOR_X_ZERO_SWITCH, INPUT); // Zero
  pinMode(MOTOR_Y_ENABLE_PIN,OUTPUT); // Enable
  pinMode(MOTOR_Y_STEP_PIN,OUTPUT); // Step
  pinMode(MOTOR_Y_DIRECTION_PIN,OUTPUT); // Dir
  pinMode(MOTOR_Y_ZERO_SWITCH, INPUT); // Zero
  pinMode(MOTOR_Z_ENABLE_PIN,OUTPUT); // Enable
  pinMode(MOTOR_Z_STEP_PIN,OUTPUT); // Step
  pinMode(MOTOR_Z_DIRECTION_PIN,OUTPUT); // Dir
  pinMode(MOTOR_Z_ZERO_SWITCH, INPUT); // Zero
  pinMode(XSM1, OUTPUT);
  pinMode(XSM2, OUTPUT);
  pinMode(YSM1, OUTPUT);
  pinMode(YSM2, OUTPUT);
  pinMode(ZSM1, OUTPUT);
  pinMode(ZSM2, OUTPUT);
  digitalWrite(XSM1, LOW);
  digitalWrite(XSM2, LOW);
  digitalWrite(YSM1, LOW);
  digitalWrite(YSM2, LOW);
  digitalWrite(ZSM1, LOW);
  digitalWrite(ZSM2, LOW);
  digitalWrite(MOTOR_X_ENABLE_PIN,LOW); // Set Enable low
  digitalWrite(MOTOR_Y_ENABLE_PIN,LOW); // Set Enable low
  digitalWrite(MOTOR_Z_ENABLE_PIN,LOW); // Set Enable low
  smallServo.attach(SMALL_CAP_PIN);
  mediumServo.attach(MEDIUM_CAP_PIN);
  largeServo.attach(LARGE_CAP_PIN);
  debugTag();
  Serial.println("Servo's setup");
  diameterMeas = sqrtFourABoverPi(CHAMBER_DIAMETER, CHAMBER_DIAMETER);
  //dht.begin();
  debugTag();
  Serial.println("DHT22 Initialized");
  init_timer3(WGM3_CTC_OCR3A,COM3_TOGGLE,COM3_NORMAL,CS3_PRESCALE_8,T3INTERUPT_ENABLE,25,25);
  //init_timer4(WGM4_CTC_OCR4A,COM4_TOGGLE,COM4_NORMAL,CS4_PRESCALE_8,T4INTERUPT_ENABLE,25,25);
  DDRC|= (1<<PORTC2) | (1<<PORTC1);
  DDRC |= (1<<PORTC0);
  DDRB |= (1<<PORTB5);
  DDRC |= (1<<PORTC6);
  EIMSK |= (1<<INT4) | (1<<INT5);
  EICRB |= (1<<ISC41) | (1<<ISC40) | (1<<ISC51) | (1<<ISC50);
  PINC= (1<<PORTC1);
  debugTag();
  Serial.println("Timer Setup");
  //motorAllZero();
  double startingHertz = 59;
  requestedTicks = htotr(startingHertz);
  requestedOverflowValue = htot(startingHertz);
  pinMode(3, INPUT);
  sei();
  debugTag();
  Serial.println("Interupts Enabled");
}

double requestedHertz = HERTZ_MAX;
double Qerror = 0;
double QerrorSum = 0;

double kp[7] = {1, 1, 1, 1, 1, 1, 1}; /* error scaling constant */
double kd[7] = {1, 1, 1, 1, 1, 1, 1}; /* error difference scaling constant */
double ki[7] = {14.07, 7.15, 6.31, 2.12, 1.59, 1.24, 0.98}; /* error sum scaling constant */

int doAdjust = 1;
int combination = 4;

void loop() {
  if(noPanic){
    if (DELAY_TIME_LOG*loopCountLogic < millis()){
      if(switchToggle){
        updateVelocity();
      }
      else{
        updatePressure();
      }
      /* Calculate base on changed values */
      #ifdef DEBUG_TAG
      debugTag();
      Serial.println("Update Temperature");
      #endif
      //currentTemperature = dht.readTemperature();
      #ifdef DEBUG_TAG
      debugTag();
      Serial.println("Update Humidity");
      #endif
      //currentHumidity = dht.readHumidity();
      #ifdef DEBUG_TAG
      debugTag();
      Serial.println("Update Dewpoint");
      #endif
      currentDewpoint = findDewpoint(currentTemperature, currentHumidity);
      #ifdef DEBUG_TAG
      debugTag();
      Serial.println("Update Fluid Density");
      #endif
      currentFluidDensity = findFluidDensity(currentTemperature, currentInsidePressureMeas , currentDewpoint); /* CONVERT PRESSURE MEAS TO UNITS FROM ANALOG */
      #ifdef DEBUG_TAG
      debugTag();
      Serial.println("Update viscocity");
      #endif
      currentViscosity = findViscocity(currentTemperature, currentInsidePressureMeas); /* CONVERT PRESSURE MEAS TO UNITS FROM ANALOG */
      currentY = calculateY(nozzleDiameter, currentInsidePressureMeas, currentFluidDensity, currentTemperature);/* CONVERT PRESSURE MEAS TO UNITS FROM ANALOG */
      #ifdef DEBUG_TAG
      debugTag();
      Serial.print("Calculate Y: ");
      Serial.println(currentY,6);
      #endif
      currentCd = calculateCd(currentCd, currentViscosity, nozzleDiameter, currentY, currentInsidePressureMeas, currentFluidDensity, currentTemperature);/* CONVERT PRESSURE MEAS TO UNITS FROM ANALOG */
      #ifdef DEBUG_TAG
      debugTag();
      Serial.print("Calculate Cd: ");
      Serial.println(currentCd,6);
      #endif
      
      currentQ = calculateQ(currentCd, currentY, nozzleArea, currentInsidePressureMeas, currentFluidDensity);/* CONVERT PRESSURE MEAS TO UNITS FROM ANALOG */
      currentQminute = currentQ * 60;
      #ifdef DEBUG_TAG
      debugTag();
      Serial.print("Calculate Q Min: ");
      Serial.println(currentQminute,6);
      #endif;
  
  
  #if 1 
      /* increment calculation */
      if(doAdjust){
        int oldCombination = combination;
        combination = (nozzleLargeOpen ? 4:0) + (nozzleMediumOpen ? 2:0) + (nozzleSmallOpen ? 1:0)-1;
        
        static double QerrorLast;
        double oldQerrorLast = QerrorLast;
        static double QerrorDiff; 
        double oldQerrorDiff = QerrorDiff;
        static double hertzSet;
        double oldHertzSet = hertzSet;
        debugTag();
        QerrorLast = Qerror;
        Qerror = currentQminute - requestedQ;
        QerrorDiff = QerrorLast-Qerror;
        
        if (oldCombination != combination){
          QerrorSum = QerrorSum * (ki[oldCombination]/ki[combination]);
        }
        else{
          QerrorSum += Qerror;
        }
        hertzSet = HERTZ_MAX-(kp[combination]*Qerror+kd[combination]*QerrorDiff+ki[combination]*QerrorSum);
        debugTag();
        Serial.print("Qerror: ");
        Serial.print(Qerror);
        Serial.print(", Qerror Last: ");
        Serial.print(QerrorLast);
        Serial.print(", Qerror Diff: ");
        Serial.print(QerrorDiff);
        Serial.print(", Qerror Sum: ");
        Serial.print(QerrorSum);
        Serial.println();
        debugTag();
        if(hertzSet >= HERTZ_MAX || hertzSet <= HERTZ_MIN){
          Serial.print("Wanted: ");
          Serial.print(hertzSet);
          Serial.print(" ,");
          QerrorLast = oldQerrorLast;
          QerrorDiff = oldQerrorDiff;
          hertzSet = oldHertzSet;
          QerrorSum -= Qerror;
        }
        
        Serial.print("Set Hertz: ");
        Serial.println(hertzSet);
        requestedHertz = hertzSet;
      /* set hertz or ticks */
      /* FIXME */
        if (requestedHertz <= HERTZ_MAX && requestedHertz >= HERTZ_MIN){
          int tempTick = htotr(requestedHertz);
          if (tempTick >= maxTicks){
            tempTick = maxTicks-1;
          }
          int tempHertz = htot(requestedHertz);
          cli();
          requestedTicks = tempTick;
          requestedOverflowValue = tempHertz;
          sei();
        }
      }
  #endif
      ++loopCountLogic;
      int tempInt = timeMovingLog/DELAY_TIME_LOG; /* fixThis */
      loopCountLogic += tempInt;
      if (tempInt){
        ++loopCountLogic;
      }
      timeMovingLog = 0;
    }
    if (DELAY_TIME_COM*loopCountCommunication < millis()){
      timeMovingCom = 0;
      /* Determine if sensor data changed */
      if (Serial.available() > 0){ /* if can send and have recieved once */
        failHigh = 0;
        failLow = 0;
        recvWithEndMarker();
        char * token;
        token = strtok(receivedChars,delimiter);
        if ((!strcmp(token,"a"))|(!strcmp(token,"A"))|(!strcmp(token,"airflow"))|(!strcmp(token,"Airflow"))){
          doAdjust = 1;
          debugTag();
          Serial.println("Adjust airflow");
          token = strtok(NULL,delimiter);
          debugTag();
          Serial.println(token);
          requestedQ = atof(token);
          motorAllSafe();
        }
        else if ((!strcmp(token,"p"))|(!strcmp(token,"P"))|(!strcmp(token,"position"))|(!strcmp(token,"Position"))){
          boolean valid = true;
          debugTag();
          Serial.println("Position");
          motorZzero();
          double tempX = 0;
          double tempY = 0;
          double tempZ = 0;
          tempX = atof(strtok(NULL,delimiter));
          if (tempX == NULL){ // no argument
            valid = false;
          }
          else{
            tempY = atof(strtok(NULL,delimiter));
          }
          if (tempY == NULL){ // no argument
            valid = false;
          }
          else{
            tempZ = atof(strtok(NULL,delimiter));
          }
          if (tempZ == NULL){ // no argument
            valid = false;
          }
          if(valid){
            motorAllMove(cmtot(tempX),cmtot(tempY),cmtot(tempZ));
          }
        }
        else if ((!strcmp(token,"px"))|(!strcmp(token,"PX"))|(!strcmp(token,"positionx"))|(!strcmp(token,"PositionX"))){
          boolean valid = true;
          debugTag();
          Serial.println("PositionX");
          //int oldZposition = locationZ;
          //motorZmove(cmtot(0));
          double tempX = 0;
          tempX = atof(strtok(NULL,delimiter));
          motorXmove(cmtot(tempX));
          //motorZmove(oldZposition);
        }
        else if ((!strcmp(token,"py"))|(!strcmp(token,"Py"))|(!strcmp(token,"positiony"))|(!strcmp(token,"PositionY"))){
          boolean valid = true;
          debugTag();
          Serial.println("PositionY");
          //int oldZposition = locationZ;
          //motorZmove(cmtot(0));
          double tempY = 0;
          tempY = atof(strtok(NULL,delimiter));
          motorYmove(cmtot(tempY));
          //motorZmove(oldZposition);
        }
        else if ((!strcmp(token,"pz"))|(!strcmp(token,"PZ"))|(!strcmp(token,"positionz"))|(!strcmp(token,"PositionZ"))){
          boolean valid = true;
          debugTag();
          Serial.println("PositionZ");
          double tempZ = 0;
          tempZ = atof(strtok(NULL,delimiter));
          motorZmove(cmtot(tempZ));
        }
        else if ((!strcmp(token,"z"))|(!strcmp(token,"Z"))|(!strcmp(token,"zero"))|(!strcmp(token,"Zero"))){
          motorAllZero();
        }
        else if ((!strcmp(token,"v"))|(!strcmp(token,"V"))|(!strcmp(token,"velocity"))|(!strcmp(token,"Velocity"))){
          debugTag();
          Serial.print("Toggle Velocity");
          digitalWrite(PRESSURE_SWITCH_PIN, HIGH);
          switchToggle = true;
        }
        else if ((!strcmp(token,"n"))|(!strcmp(token,"N"))|(!strcmp(token,"nozzle"))|(!strcmp(token,"Nozzle"))){
          debugTag();
          Serial.print("Toggle Nozzle");
          digitalWrite(PRESSURE_SWITCH_PIN, LOW);
          switchToggle = false;
        }
        #if 1 /* DO NOT USE WITH AUTORANGING */
        else if ((!strcmp(token,"f"))|(!strcmp(token,"F"))|(!strcmp(token,"fan"))|(!strcmp(token,"Fan"))){
          doAdjust = 0;
          double temp;
          char* tempToken = strtok(NULL,delimiter);
          debugTag();
          Serial.print("Recieved: ");
          Serial.println(tempToken);
          temp = atof(tempToken);
          requestedHertz = temp;
          debugTag();
          Serial.print("Requested Fan Hertz: ");
          Serial.print(requestedHertz);
          if (requestedHertz <= HERTZ_MAX && requestedHertz >= HERTZ_MIN){
            QerrorSum = 60-requestedHertz;
            int tempTick = htotr(requestedHertz);
            if (tempTick >= maxTicks){
              tempTick = maxTicks-1;
            }
            int tempHertz = htot(requestedHertz);
            cli();
            requestedTicks = tempTick;
            requestedOverflowValue = tempHertz;
            sei();
            Serial.println(" Hz");
          }
          else{
            Serial.println(", Invalid Request");  
          }
        }
        #endif
        else{
          debugTag();
          Serial.println("Unrecognized Command");
        }
        if (newData == true) {
          debugTag();
          Serial.print("Recieved Charecters: ");
          Serial.println(receivedChars);
          newData = false; /* IMPORTANT */
        }
        #ifdef DEBUG_TAG
        debugTag();
        Serial.print("currentQminute: ");
        Serial.println(currentQminute); 
        #endif
        if (requestedQ < 0.14544){/* less than min */
        debugTag();
          Serial.println("State 1");
          nozzleSmallOpen = true;
          nozzleMediumOpen = false;
          nozzleLargeOpen = false;
        }
        else if (requestedQ < 0.32397){
          debugTag();
          Serial.println("State 2");
          nozzleSmallOpen = true;
          nozzleMediumOpen = false;
          nozzleLargeOpen = false;
        }
        else if (requestedQ < 0.653505){
          debugTag();
          Serial.println("State 3");
          nozzleSmallOpen = false;
          nozzleMediumOpen = true;
          nozzleLargeOpen = false;
        }
        else if (requestedQ < 1.0113){
          debugTag();
          Serial.println("State 4");
          nozzleSmallOpen = true;
          nozzleMediumOpen = true;
          nozzleLargeOpen = false;
        }
        else if (requestedQ < 2.1621){
          debugTag();
          Serial.println("State 5");
          nozzleSmallOpen = false;
          nozzleMediumOpen = false;
          nozzleLargeOpen = true;
        }
        else if (requestedQ < 2.4915){
          debugTag();
          Serial.println("State 6");
          nozzleSmallOpen = true;
          nozzleMediumOpen = false;
          nozzleLargeOpen = true;
        }
        else if (requestedQ < 2.8821){
          debugTag();
          Serial.println("State 7");
          nozzleSmallOpen = false;
          nozzleMediumOpen = true;
          nozzleLargeOpen = true;
        }
        else if (requestedQ < 3.3066){
          debugTag();
          Serial.println("State 8");
          nozzleSmallOpen = true;
          nozzleMediumOpen = true;
          nozzleLargeOpen = true;
        }
        else{ /* to high or something wrong */
          debugTag();
          Serial.println("State 9");
          nozzleSmallOpen = true;
          nozzleMediumOpen = true;
          nozzleLargeOpen = true;
        }
        nozzleArea = (nozzleSmallOpen ? NOZZLE_SMALL_AREA : 0) + (nozzleMediumOpen ? NOZZLE_MEDIUM_AREA : 0) + (nozzleLargeOpen ? NOZZLE_LARGE_AREA : 0);
        nozzleDiameter = sqrt(4*nozzleArea/3.141519);
        if (nozzleSmallOpen){
          debugTag();
          Serial.println("Open: Small");
          smallPos = servoOpen(smallPos, SMALL_MIN_POS, SMALL_MAX_POS, smallServo);
        }
        if (nozzleMediumOpen){
          debugTag();
          Serial.println("Open: Medium");
          mediumPos = servoOpen(mediumPos, MEDIUM_MIN_POS, MEDIUM_MAX_POS, mediumServo);
        }
        if (nozzleLargeOpen){
          debugTag();
          Serial.println("Open: Large");
          largePos = servoOpen(largePos, LARGE_MIN_POS, LARGE_MAX_POS, largeServo);
        }
        if (!nozzleSmallOpen){
          debugTag();
          Serial.println("Close: Small");
          smallPos = servoClose(smallPos, SMALL_MIN_POS, SMALL_MAX_POS, smallServo);
        }
        if (!nozzleMediumOpen){
          debugTag();
          Serial.println("Close: Medium");
          mediumPos = servoClose(mediumPos, MEDIUM_MIN_POS, MEDIUM_MAX_POS, mediumServo);
        }
        if (!nozzleLargeOpen){
          debugTag();
          Serial.println("Close: Large");
          largePos = servoClose(largePos, LARGE_MIN_POS, LARGE_MAX_POS, largeServo);
        }
      }
  
      nozzleVelocity = currentQ/(nozzleArea*currentCd*currentY);
  
      logTag();
      Serial.print(requestedQ,3); /* Requested Airflow */
      Serial.print(", ");
      if (!switchToggle){
        Serial.print(currentQminute,3); /* Actual airflow */
      }
      Serial.print(", ");
      Serial.print(currentViscosity, 8); /* Viscocity */
      Serial.print(", ");
      Serial.print(currentFluidDensity, 2); /* Fluid Density */
      Serial.print(", ");
      Serial.print(currentDewpoint, 1); /* Dewpoint */
      Serial.print(", ");
      Serial.print(currentHumidity, 1); /* Humidity */
      Serial.print(", ");
      Serial.print(currentTemperature, 1); /* Temperature */
      Serial.print(", ");
      Serial.print(ttocm(locationX),3); /* X position */
      Serial.print(", ");
      Serial.print(ttocm(locationY),3); /* Y position */
      Serial.print(", ");
      Serial.print(ttocm(locationZ),3); /* Z position */
      Serial.print(", ");
      if (!switchToggle){
        Serial.print(currentPressureDifferenceMeas,3); /* PressureDif */
      }
      Serial.print(", ");
      if (!switchToggle){
        Serial.print(currentInsidePressureMeas,3); /* Inside Pressure */
      }
      Serial.print(", ");
        Serial.print(currentOutsidePressureMeas,3); /* Outside Pressure */
      Serial.print(", ");
      if (switchToggle){
        Serial.print(currentVelocityPressureMeas,3); /* Velocity Pressure */
      }
      Serial.print(", ");
      if (switchToggle){
        Serial.print(currentVelocity,3); /* Velocity */
      }
      Serial.print(", ");
      if (!switchToggle){
        Serial.print(nozzleVelocity,3);
      }
      Serial.print(", ");
      Serial.print(nozzleDiameter,5);
      Serial.print(", ");
      Serial.print(nozzleArea,6);
      Serial.print(", ");
      Serial.print(requestedHertz,3);
      Serial.println();
      
      debugTag();
      Serial.println("End of Logic");
      ++loopCountCommunication;
      int tempInt = timeMovingCom/DELAY_TIME_COM;
      loopCountCommunication += tempInt;
      if (tempInt){
        ++loopCountCommunication;
      }
    }
  }
  else{
    cli();
  }
}
