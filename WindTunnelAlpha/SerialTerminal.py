import serial
import time

ser = serial.Serial('/dev/ttyACM0', 9600, timeout=1)

var = input("Enter something: ")
ser.write(str.encode(var + '\n')) 

def main():
    while 1:
        try:
            msg = ser.readline()
            if msg:
                print('Recieved: ')
                print (msg)
            else:
                var = input("Enter something: ")
                ser.write(str.encode(var + '\n')) 
        except ser.SerialTimeoutException:
            print('Data could not be read')

while True:
    main()
