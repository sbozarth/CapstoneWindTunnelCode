# Name: Daniel Bozarth
# Date: 2018-9-27
# Modification History:
# Initial Revision (2018-9-13)
# Working Dependencies (2018-9-20)
# Multiple Files for Utility Functions (2018-9-27)
#have to set src in command
headers = $(wildcard *.h)
headersrc = $(headers:.h=.c)
headerobj = $(headers:.h=.o)
objFile = $(src:.c=.o)
elfFile = $(objFile:.o=.elf)
hexFile = $(objFile:.o=.hex)

CC = avr-gcc
OBJCOPY = avr-objcopy
FLASH = avrdude
CFLAGS += -g -Os -mmcu=atmega2560

flash: $(hexFile)
	$(FLASH) -p atmega2560 -c usbtiny -e -U flash:w:$(hexFile)

$(hexFile): $(elfFile)
	$(OBJCOPY) -j .text -j .data -O ihex $(elfFile) $(hexFile)

$(elfFile): $(objFile) $(headerobj)
	$(CC) -g -Os -mmcu=atmega2560 -o $(elfFile) $(objFile) $(headerobj)

$(objFile): $(src) $(headerobj)
	$(CC) -g -Os -mmcu=atmega2560 -c $(src)
	

.PHONY: clean
clean:
	rm -f $(objFile)
	rm -f $(elfFile)
	rm -f $(hexFile)
	rm -f $(headerobj)
