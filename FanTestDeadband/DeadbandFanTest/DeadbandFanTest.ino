#include "DeadbandFanTest.h"

#include <avr/io.h>
#include <avr/interrupt.h>

#include <stdint.h>
#include <math.h>

#define PRESSURE_PIN_INSIDE A0
#define PRESSURE_PIN_OUTSIDE A1

#define ANALOG_MEASURMENT_RANGE 1024
#define INSIDEPRESSURE_INWC_RANGE 5
#define INSIDEPRESSURE_PASCAL_RANGE INSIDEPRESSURE_INWC_RANGE*INWC_TO_PASCAL
#define OUTSIDEPRESSURE_INWC_RANGE 3
#define OUTSIDEPRESSURE_PASCAL_RANGE OUTSIDEPRESSURE_INWC_RANGE*INWC_TO_PASCAL

#define INWC_TO_PASCAL 248.8271

double currentInsidePressureMeas = 0;
double currentOutsidePressureMeas = 0;
double currentPressureDifferenceMeas;

double artopa(int measurment, double range){ /* analog pressure measurement reading to actual */
  return (1.0/ANALOG_MEASURMENT_RANGE) * (range/5) * measurment;
}

void updatePressure(){
  currentInsidePressureMeas = artopa(analogRead(PRESSURE_PIN_INSIDE), INSIDEPRESSURE_PASCAL_RANGE );
  Serial.print("Inside pressure ");
  Serial.print(currentInsidePressureMeas);
  Serial.print(" /1024, \n");
  currentOutsidePressureMeas = artopa(analogRead(PRESSURE_PIN_OUTSIDE), OUTSIDEPRESSURE_PASCAL_RANGE );
  Serial.print("Outside pressure ");
  Serial.print(currentOutsidePressureMeas);
  Serial.print(" /1024, \n");
  currentPressureDifferenceMeas = currentOutsidePressureMeas - currentInsidePressureMeas;
  if (currentPressureDifferenceMeas < 0){
    currentPressureDifferenceMeas = currentPressureDifferenceMeas *-1;
    Serial.println("Error(updatePressure): inside pressure measured greater than outside pressure");  
  }
  if (currentPressureDifferenceMeas == 0){
    currentPressureDifferenceMeas = 0.0000001; 
  }
}

void debugTag(){
  Serial.print("DEBUG:");
  Serial.print(millis()/1000.0,3);
  Serial.print(": ");
}

void logTag(){
  Serial.print("LOG:");
  Serial.print(millis()/1000.0,3);
  Serial.print(", ");
}

void init_timer3(wgm3_t wgm3, com3_t com3a, com3_t com3b, cs3_t cs3, t3interupt_t t3interupt, uint8_t ocr3a, uint8_t ocr3b){
  TCCR3A = ((com3a&0x3)<<COM3A0) | ((com3b&0x3)<<COM3B0) | ((wgm3&0x3)<<WGM30);
  
  TCCR3B = ((wgm3&0xC)<<WGM32>>2) | ((cs3&0x7)<<CS30);
  
  OCR3A = ocr3a;
  
  OCR3B = ocr3b;
  
  TIMSK3 = (1<<OCIE1B);//(1<<OCIE3A);
}

int ammount = 0;
#define OVERFLOW_MAX 7677
#define OVERFLOW_MIN 1279
#define OVERFLOW_MIDDLE 2559
int overflowValue = OVERFLOW_MIDDLE;
int overflowHalfway = overflowValue>>1;
int requestedOverflowValue = overflowValue;
int deadband = overflowValue/5;

ISR (TIMER3_COMPB_vect)
{
  ++ammount;
  if (ammount >= overflowValue){
    PINC = (1<<PORTC1);
    ammount = 0;
    if (overflowValue != requestedOverflowValue){
      overflowValue = requestedOverflowValue;
      overflowHalfway = overflowValue>>1;
      deadband = overflowValue/5;
    }
  }
  else if (ammount == overflowValue-deadband){
    PINC = (1<<PORTC2);
  }
  else if (ammount == overflowHalfway-deadband){
    PINC = (1<<PORTC1);
  }
  else if (ammount == overflowHalfway){
    PINC = (1<<PORTC2);
  }
}

int fanAmmount = 0;
int currentHertzFan = 0;
int requestedCurrentHertzFan = 0;

ISR (INT4_vect){
  cli();
  ++fanAmmount;
  if (fanAmmount >= 60){
    fanAmmount = 0;
    if (requestedCurrentHertzFan != currentHertzFan){
      currentHertzFan = requestedCurrentHertzFan;
    }
  }
  else if (fanAmmount <= currentHertzFan){
    PORTC |= 1<<PORTC7;
  }
  else{
    PORTC &= ~(1<<PORTC7);
  }
  sei();
}

const byte numChars = 16;
char receivedChars[numChars];
boolean newData;

void recvWithEndMarker() {
  static byte ndx = 0;
  char endMarker = '\n';
  char rc;
 
  if (Serial.available() > 0) {
    while (Serial.available() > 0 && newData == false) {
      rc = Serial.read();
      if (rc != endMarker) {
        //Serial.println("!");
        receivedChars[ndx] = rc;
        ndx++;
        if (ndx >= numChars) {
          ndx = numChars - 1;
        }
      }
      else {
        receivedChars[ndx] = '\0'; // terminate the string
        ndx = 0;
        newData = true;
        
      }
    }
  }
}

#define TICK_CONVERSION_FACTOR 76767
#define DEADBAND_CONVERSION_FACTOR 15353

double ttoh(int tick){
  /* For 20kHz tick */
  return (double)TICK_CONVERSION_FACTOR/tick;
}

int htot(double hertz){
  /* For 20kHz tick */
  return round(TICK_CONVERSION_FACTOR/hertz);
}

int htodb(double hertz){
  return round(DEADBAND_CONVERSION_FACTOR/hertz);
}

int ttodb(int tick){
  return tick/5;  
}

void setup(){
  Serial.begin(9600);
  Serial1.begin(9600);
  init_timer3(WGM3_CTC_OCR3A,COM3_TOGGLE,COM3_NORMAL,CS3_PRESCALE_8,T3INTERUPT_ENABLE,25,25);
  DDRC|= (1<<PORTC2) | (1<<PORTC1);
  DDRC |= (1<<PORTC0);
  DDRB |= (1<<PORTB5);
  
  
  DDRC |= (1<<PORTC7);
  EIMSK |= (1<<INT4);
  EICRB |= (1<<ISC41) | (1<<ISC40);
  
  PINC= (1<<PORTC1);
  sei();
}

double requestedHertz = 30;

void loop() {
  if (Serial.available() > 0){
    recvWithEndMarker();
    if (newData == true){
      requestedHertz = atof(receivedChars);
      debugTag();
      Serial.print("Requested: ");
      Serial.print(requestedHertz);
      Serial.print("Hz, ");
      requestedOverflowValue = htot(requestedHertz);
      Serial.print(requestedOverflowValue);
      Serial.print(" ticks, ");
      Serial.print("Fan Hertz: ");
      fanAmmount = 0;
      PORTC &= ~(1<<PORTC7);
      cli();
      requestedCurrentHertzFan = (int)requestedHertz;
      if (requestedCurrentHertzFan >= 60){
        PORTC |= (1<<PORTC7);
        EIMSK &= ~(1<<INT4);  
      }
      else{
        PORTC &= ~(1<<PORTC7);
        EIMSK |= (1<<INT4);
      }
      sei();
      fanAmmount = 0;
      Serial.print(requestedCurrentHertzFan);
      Serial.print("Hz, ");
      Serial.println();
      newData = false;
    }
  }
  delay(1000);
}

