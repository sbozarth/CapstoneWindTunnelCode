//
// Copyright (c) 2018 Christopher Baker <https://christopherbaker.net>
// Modified: Daniel Bozarth, 2019
// SPDX-License-Identifier:  MIT
//


#include <MCP3XXX.h>


MCP3202 adc0;
MCP3202 adc1;


void setup()
{
  Serial.begin(9600);

  // Use the default SPI hardware interface.
  adc0.begin(9,11,12,13);
  adc1.begin(10,11,12,13);

  // Or use custom pins to use a software SPI interface.
  // adc.begin(SS, MOSI, MISO, SCK);
}

void loop()
{
  Serial.print("ADC 0: ");
  for (size_t i = 0; i < adc0.numChannels(); ++i)
  {
    Serial.print(adc0.analogRead(i)>>2);

    if (i == adc0.numChannels() - 1)
    {
      Serial.println();
    }
    else
    {
      Serial.print(",");
    }
  }
  
  Serial.print("ADC 1: ");
  for (size_t i = 0; i < adc1.numChannels(); ++i)
  {
    Serial.print(adc1.analogRead(i)>>2);

    if (i == adc1.numChannels() - 1)
    {
      Serial.println();
    }
    else
    {
      Serial.print(",");
    }
  }
  
  delay(1000);
}
