/* Wind Tunnel Arduino Code                                   */
/* Serial Communication and sensor sampling                   */
/* Arduino MEGA 2560                                          */
/* End Goal: Get rid of all arduino specific code, write in c */
/*           Get rid of all floats and division possible      */

#include <stdint.h>
#include <math.h>
#include <stdfix.h>
#include <avr/sleep.h>

#include "uart.h"


unsigned char incomingByte = 0;

_Accum calculateTempCd(_Accum Re){
  return (_Accum)0.9986 - (_Accum)7.007/sqrt(Re) + (_Accum)134.6/Re;
}

_Accum sqrtFourABoverPi(_Accum diameterA, _Accum diameterB){
  return sqrt(((_Accum)1.2732395*diameterA*diameterB)); /* float is pi/4 */
}

/* nozzleDiameter/diameterMeas ^ 4 can be calculated, and passed in when the nozzles are open */
_Accum calculateY(_Accum nozzleDiameter, _Accum pressureChange, _Accum fluidDensity, _Accum gasConstant, _Accum temperature, _Accum diameterMeas){
  return 1-((_Accum)0.548+(_Accum)0.71*(nozzleDiameter/diameterMeas)*(nozzleDiameter/diameterMeas)*(nozzleDiameter/diameterMeas)*(nozzleDiameter/diameterMeas))*(pressureChange/(fluidDensity*gasConstant*(temperature+(_Accum)273.15)));
}

/* sqrt(2)/viscocity  could be pre calculated and passed in, upated only when viscocity changes */
_Accum calculateRe(_Accum Cd, _Accum viscocity, _Accum nozzleDiameter, _Accum Y, _Accum pressureChange, _Accum fluidDensity, _Accum diameterMeas){
  return ((_Accum)1.41421356/viscocity)*Cd*Y*sqrt((pressureChange*fluidDensity)/(1-1*diameterMeas)); /* float is sqrt(2) */
}

_Accum calculateCd(_Accum Cd, _Accum viscocity, _Accum nozzleDiameter, _Accum Y, _Accum pressureChange, _Accum fluidDensity, _Accum gasConstant, _Accum temperature, _Accum diameterA, _Accum diameterB){
  _Accum oldCd = 0.99;
  _Accum diameterMeas = sqrtFourABoverPi(diameterA, diameterB);
  _Accum Re = calculateRe(Cd,viscocity,nozzleDiameter,Y,pressureChange,fluidDensity,diameterMeas);
  _Accum newCd = calculateTempCd(Re);
  _Accum cdChange = oldCd-newCd;
  while (!((cdChange < (_Accum)0.001) && (cdChange > (_Accum)-0.001))){
    oldCd = newCd;
    diameterMeas = sqrtFourABoverPi(diameterA, diameterB);
    Re = calculateRe(Cd,viscocity,nozzleDiameter,Y,pressureChange,fluidDensity,diameterMeas);
    newCd =calculateTempCd(Re);
    cdChange = oldCd-newCd;
  }
  return Re;
}

/* nozzleDiameter/chamber diameter can be pre calculated and passed in when the open nozzles are set */
_Accum calculateQ(_Accum Cd, _Accum Y, _Accum nozzleDiameter, _Accum pressureChange, _Accum fluidDensity, _Accum chamberDiameter){
  return Cd*Y*(_Accum)0.78539816*nozzleDiameter*nozzleDiameter*sqrt((2*pressureChange)/(fluidDensity*(1-(nozzleDiameter/chamberDiameter)*(nozzleDiameter/chamberDiameter)*(nozzleDiameter/chamberDiameter)*(nozzleDiameter/chamberDiameter))));
}

/* Lookup table or equation */
_Accum findFluidDensity(_Accum temperature,_Accum pressure,_Accum dewpoint){
  
}

/* Lookup table or equation */
_Accum findViscocity(_Accum temperature,_Accum pressure){
  
}

void init(){
	uart_init();
}

void main() {
	while (1)
	#if 0
	/* If diameter of open nozzles changes recalculate values,                           Y, Cd, Q */
	/* If change in pressure changes recalculate values,        fluidDensity, viscocity, Y, Cd, Q */
	/* if temp changes recalculate values,                      fluidDensity, viscocity, Y, Cd, Q */
	/* if dewpoint changes recalculate,                         fluidDensity,            Y, Cd, Q */
	/* Check for user input of desired q */
	/* If input calculate which nozzles probably need to be opened, open them, close others, set related values */
	 
	/* Read Sensors */
	/* Store time and location of read */
	  
	/* Send Sensor Data */
	 
	/* Determine if sensor data changed */
	
	/* Calculate base on changed values */
	currentFluidDensity = findFluidDensity();
	currentViscosity = findViscocity();
	currentY = calculateY();
	currentCd = calculateCd();
	currentQ = calculateQ();
	
	/* Change fanspeed to approach desired q */
	/* Remember to avoid oscilating around the desired value */
	/* Send Data */
	if (Serial.available() > 0) {
	/* Currently open nozzles */
	uart_send(); /* 3 bit number with each bit corresponding to nozzle */
	/* Requested Q */
	uart_send();
	/* Current Q */
	uart_send();
	/* All pressure sensor's status */
	uart_send();
	/* Moving sensors location */
	uart_send();
	/* Current change in pressure */
	uart_send();
	/* Current temp */
	uart_send();
	/* Current dewpoint */
	uart_send();
	/* current fluid density */
	uart_send();
	/* current viscocity */
	uart_send();
	/* End Line */
	uart_send("\n");
	 }
	#endif
	#if 1
	if (uart_has_data()) {
	// read the incoming byte:
	/* Need to collect all incomming and then send string back */
		incomingByte = uart_get_char();
		
		// say what you got:
		uart_send(&incomingByte);
		
	}
	/*
	else {
		Serial.println(incomingByte);
		_delay_ms(200); 
	}*/
	#endif
}
