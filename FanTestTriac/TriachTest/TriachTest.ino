#include "TriachTest.h"
#include <avr/io.h>
#include <avr/interrupt.h>

void init_timer3(wgm3_t wgm3, com3_t com3a, com3_t com3b, cs3_t cs3, t3interupt_t t3interupt, uint8_t ocr3a, uint8_t ocr3b){
  TCCR3A = ((com3a&0x3)<<COM3A0) | ((com3b&0x3)<<COM3B0) | ((wgm3&0x3)<<WGM30);
  
  TCCR3B = ((wgm3&0xC)<<WGM32>>2) | ((cs3&0x7)<<CS30);
  
  OCR3A = ocr3a;
  
  OCR3B = ocr3b;
  
  TIMSK3 = (1<<OCIE1B);//(1<<OCIE3A);
}

void init_timer4(wgm4_t wgm4, com4_t com4a, com4_t com4b, cs4_t cs4, t4interupt_t t4interupt, uint8_t ocr4a, uint8_t ocr4b){
  TCCR4A = ((com4a&0x3)<<COM4A0) | ((com4b&0x3)<<COM4B0) | ((wgm4&0x3)<<WGM40);
  
  TCCR4B = ((wgm4&0xC)<<WGM42>>2) | ((cs4&0x7)<<CS40);
  
  OCR4A = ocr4a;
  
  OCR4B = ocr4b;
  
  TIMSK4 = (1<<OCIE1B);//(1<<OCIE3A);
}

int ammount = 0;
#define OVERFLOW_MAX 7677
#define OVERFLOW_MIN 1279
#define OVERFLOW_MIDDLE 2559
int overflowValue = OVERFLOW_MIDDLE;
int overflowHalfway = overflowValue>>1;
int requestedOverflowValue = overflowValue;
int deadband = overflowValue/5;

int tickAmmount = 0;
int maxTicks = 639;
int requestedTicks = 500;
int currentRequestedTicks = requestedTicks;

ISR (TIMER3_COMPB_vect)
{
  ++ammount;
  if (ammount >= overflowValue){
    PINC = (1<<PORTC1);
    ammount = 0;
    if (overflowValue != requestedOverflowValue){
      overflowValue = requestedOverflowValue;
      overflowHalfway = overflowValue>>1;
      deadband = overflowValue/5;
    }
  }
  else if (ammount == overflowValue-deadband){
    PINC = (1<<PORTC2);
  }
  else if (ammount == overflowHalfway-deadband){
    PINC = (1<<PORTC1);
  }
  else if (ammount == overflowHalfway){
    PINC = (1<<PORTC2);
  }
  
  ++tickAmmount;
  if (tickAmmount == (maxTicks-currentRequestedTicks)){
    currentRequestedTicks = requestedTicks;
    PORTC |= (1<<PORTC6);
  }
  if(tickAmmount == maxTicks){
    PORTC &= ~(1<<PORTC6);
  }
  
}
#if 0
ISR (TIMER4_COMPB_vect){
  //PORTC ^= (1<<PORTC7);
  ++tickAmmount;
  if (tickAmmount == currentRequestedTicks){
    PORTC &= ~(1<<PORTC6);
  }
}
#endif
ISR (INT4_vect){
  PORTC &= ~(1<<PORTC6);
  tickAmmount = 0;
  currentRequestedTicks = requestedTicks;
}

const byte numChars = 32;
char receivedChars[numChars]; // an array to store the received data
boolean newData = false;

void recvWithEndMarker() {
  static byte ndx = 0;
  char endMarker = '\n';
  char rc;
 
  if (Serial.available() > 0) {
    while (Serial.available() > 0 && newData == false) {
      rc = Serial.read();
      if (rc != endMarker) {
        //Serial.println("!");
        receivedChars[ndx] = rc;
        ndx++;
        if (ndx >= numChars) {
          ndx = numChars - 1;
        }
      }
      else {
        receivedChars[ndx] = '\0'; // terminate the string
        ndx = 0;
        newData = true;
        
      }
    }
  }
}

#define TICK_CONVERSION_FACTOR 76767
#define DEADBAND_CONVERSION_FACTOR 15353

double ttoh(int tick){
  /* For 20kHz tick */
  return (double)TICK_CONVERSION_FACTOR/tick;
}

int htot(double hertz){
  /* For 20kHz tick */
  return round(TICK_CONVERSION_FACTOR/hertz);
}

int htodb(double hertz){
  return round(DEADBAND_CONVERSION_FACTOR/hertz);
}

int ttodb(int tick){
  return tick/5;  
}

void setup(){
  Serial.begin(115200);
  Serial1.begin(115200);
  Serial.println("Initialize");
  init_timer3(WGM3_CTC_OCR3A,COM3_TOGGLE,COM3_NORMAL,CS3_PRESCALE_8,T3INTERUPT_ENABLE,25,25);
  //init_timer4(WGM4_CTC_OCR4A,COM4_TOGGLE,COM4_NORMAL,CS4_PRESCALE_8,T4INTERUPT_ENABLE,25,25);
  DDRC|= (1<<PORTC2) | (1<<PORTC1);
  DDRC |= (1<<PORTC0);
  DDRB |= (1<<PORTB5);
  DDRC |= (1<<PORTC6);
  EIMSK |= (1<<INT4);
  EICRB |= (1<<ISC41) | (1<<ISC40);
  PINC= (1<<PORTC1);
  sei();
}

double requestedHertz = 60;
char delimiter[3] = ", ";

void loop(){
  if (Serial.available() > 0){
    recvWithEndMarker();
    char * token;
    token = strtok(receivedChars,delimiter);
    if (newData == true){
      #if 0
      if(!strcmp(token,"f")){
        token = strtok(NULL,delimiter);
        int tempHertz = htot(atof(token));
        requestedOverflowValue =3030 tempHertz;
        Serial.print("Requested ");
        Serial.print(token);
        Serial.println(" hz");
      }
      else if(!strcmp(token,"t")){
        token = strtok(NULL,delimiter);
        int tempTick = maxTicks*(atof(token)/100);
        requestedTicks = tempTick;
        Serial.print("Requested ");
        Serial.print(token);
        Serial.println(" percent");
      }
      newData = false; /* IMPORTANT */
      #endif
      #if 1
      requestedHertz = atof(receivedChars);
      
      Serial.print("Requested: ");
      Serial.print(requestedHertz);
      Serial.print("Hz, ");
      if (requestedHertz <= 60.0 && requestedHertz >= 10){
        double requestedHertzSquare = requestedHertz * requestedHertz;
        double requestedHertzCube = requestedHertzSquare * requestedHertz;
        int tempTick = (0.0000048*requestedHertzCube-0.000317297*requestedHertzSquare+0.010164001*requestedHertz)*maxTicks;
        
        if (tempTick >= maxTicks){
          tempTick = maxTicks-1;
        }
        int tempHertz = htot(requestedHertz);
        cli();
        requestedTicks = tempTick;
        requestedOverflowValue = tempHertz;
        sei();
        Serial.print(requestedTicks);
        Serial.print(" Ticks, ");
        Serial.print(requestedOverflowValue);
        Serial.print(" Ticks, ");
      }
      else{
        Serial.print("Invalid Request");
      }
      Serial.println();
      newData = false;
      #endif
    }
  }
  delay(1000);
}
